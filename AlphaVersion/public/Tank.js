class Tank {
    constructor(tankClone) {
        this.ping = 50;
        this.getDataFromClone(tankClone);
        this.playSoundExplotion = true;
    }

    getDataFromClone(tankClone) {
        for (const prop in tankClone) {
            if (prop === 'weapon') {
                this[prop] = new Weapon(tankClone[prop]);
            } else if (prop === 'ping' && Stage.Instance.mainPlayer !== undefined && tankClone.id === Stage.Instance.mainPlayer.id) {
                continue;
            } else {
                this[prop] = tankClone[prop];
            }
        }
    }

    destroy(damage) {
        this.shield -= damage;
        if (this.shield < 0) {
            this.life += this.shield;
            this.shield = 0;
            if (this.life < 0) {
                this.destroyed = true;
                this.rate = 0;
                this.imgActual = 1;
            }
        }
    }

    handleKeysPressed() {
        let mapKeys = this.mapKeys;
        this.rSpeed = 0;
        this.xMove = 0;
        this.yMove = 0;
        if (mapKeys.DOWN || mapKeys.UP) {
            this.rate += 1;
            if (this.rate > 4) {
                this.rate = 0;
                this.imgActual += mapKeys.DOWN ? 1 : -1;
                if (this.imgActual > this.cantimages) {
                    this.imgActual = 1;
                } else if (this.imgActual < 1) {
                    this.imgActual = this.cantimages;
                }
            }
            if (mapKeys.UP && (mapKeys.LEFT || mapKeys.RIGHT)) {
                let dr = 0;
                if (mapKeys.LEFT) {
                    dr -= this.rotationSpeed;
                }
                if (mapKeys.RIGHT) {
                    dr += this.rotationSpeed;
                }
                this.rSpeed = dr;
            } else if (mapKeys.DOWN && (mapKeys.LEFT || mapKeys.RIGHT)) {
                let dr = 0;
                if (mapKeys.LEFT) {
                    dr += this.rotationSpeed;
                }
                if (mapKeys.RIGHT) {
                    dr -= this.rotationSpeed;
                }
                this.rSpeed = dr;
            }
            this.xMove += (mapKeys.UP === true ? 1 : -1) * this.linearSpeed * Math.cos(this.rotation);
            this.yMove += (mapKeys.UP === true ? 1 : -1) * this.linearSpeed * Math.sin(this.rotation);
            this.moving = true;
        } else if (mapKeys.LEFT || mapKeys.RIGHT) {
            let dr = 0;
            if (mapKeys.LEFT) {
                dr -= this.rotationSpeed;
            }
            if (mapKeys.RIGHT) {
                dr += this.rotationSpeed;
            }
            this.rSpeed = dr;
            this.moving = true;
        }
        //90 es la z
        //this.playNoAmoSnd = false;
        if (mapKeys.SHOOT) {
            if (this.weapon.fireRateCounter > this.weapon.limitFireRate && this.weapon.munition > 0) {
                this.weapon.munition--;
                this.weapon.fireRateCounter = 0;
            }
        }
    }
    handleKeysReleased(mapKeys) {
        if (!(mapKeys.UP || mapKeys.DOWN || mapKeys.LEFT || mapKeys.RIGHT)) {
            this.moving = false;
        }
        //if (this.mapKeys !== undefined && this.mapKeys.SHOOT && !mapKeys.SHOOT) {
        //    this.weapon.fireRateCounter = this.weapon.limitFireRate + 1;
        //}
        this.mapKeys = mapKeys;
    }

    update() {
        if (!this.destroyed) {
            this.playSoundExplotion = true;
            if (this.mapKeys !== undefined) {
                this.handleKeysPressed();
            }
            if (this.weapon.fireRateCounter <= this.weapon.limitFireRate) {
                ++this.weapon.fireRateCounter;
            }
            if (this.weapon.munition <= 0) {
                if (this.weapon.reloadRate > this.weapon.reloadRateLimit) {
                    this.weapon.reloadRate = 0;
                    this.weapon.reloadCounter++;
                    if (this.weapon.reloadCounter === this.weapon.originalMunition) {
                        this.weapon.munition = this.weapon.originalMunition;
                        this.weapon.reloadCounter = 0;
                    }
                } else {
                    this.weapon.reloadRate++;
                }
            }
            this.rotation += this.rSpeed;
            let xEvaluated = this.x + this.xMove;
            let yEvaluated = this.y + this.yMove;
            let colli = Map.Instance().getThereACollidable(xEvaluated, yEvaluated, this.w + 5, this.h + 5);
            let otherTank = Map.Instance().getATank(xEvaluated, yEvaluated, this.w + 5, this.h + 5, this.id);
            if (colli === null && otherTank === null) {
                this.x += this.xMove;
                this.y += this.yMove;
            } else {
                /*this.rSpeed += this.rotationSpeed*(-1)+0.3*Math.sign(this.rotationSpeed);*/
                /*puntos medios  cuadrado tanque*/
                xEvaluated = this.x + this.xMove;
                yEvaluated = this.y;

                otherTank = Map.Instance().getATank(xEvaluated, yEvaluated, this.w + 5, this.h + 5, this.id);
                colli = Map.Instance().getThereACollidable(xEvaluated, yEvaluated, this.w + 5, this.h + 5);
                if (colli === null && otherTank === null) {
                    this.x += this.xMove;
                } else {
                    xEvaluated = this.x;
                    yEvaluated = this.y + this.yMove;
                    otherTank = Map.Instance().getATank(xEvaluated, yEvaluated, this.w + 5, this.h + 5, this.id);
                    colli = Map.Instance().getThereACollidable(xEvaluated, yEvaluated, this.w + 5, this.h + 5);
                    if (colli === null && otherTank === null) {
                        this.y += this.yMove;
                    }
                }
            }
            //let pu = Map.Instance().getAPowerUp(xEvaluated, yEvaluated, this.w, this.h);
            //if (pu !== null) {
            //    pu.getPower.call(this);
            //    pu.deleteEntity = true;
            //}
        } else {
            if (!this.reappear) {
                if (this.playSoundExplotion) {
                    SoundManager.Instance.stopSound('explosion');
                    SoundManager.Instance.stopSound('tankExplosion');
                    SoundManager.Instance.playSound('tankExplosion', false);
                    this.playSoundExplotion = false;
                    console.log('Exploto sonido');
                }
                this.rate += 1;
                if (this.rate > 6) {
                    this.rate = 0;
                    this.imgActual += 1;
                    if (this.imgActual > this.cantimagesDestruction) {
                        this.imgActual = 1;
                        this.life = this.originalLife;
                        this.weapon.munition = this.weapon.originalMunition;
                        this.reappear = true;
                    }
                }
            }
        }
    }
    playSounds() {
        if (!this.destroyed) {
            if (InputManager.Instance.mapKeysPressed.SHOOT && this.weapon.munition === 0) {
                SoundManager.Instance.playSound('noBullets', false);
            }
            if (this.moving) {
                SoundManager.Instance.stopSound('engine');
                SoundManager.Instance.playSound('movetank', true);
            } else {
                SoundManager.Instance.stopSound('movetank');
                SoundManager.Instance.playSound('engine', true);
            }
        } else {
            SoundManager.Instance.stopSound('engine');
            SoundManager.Instance.stopSound('movetank', true);
        }
    }
    render() {
        if (!this.destroyed) {
            push();
            translate(this.x, this.y);

            if (this.shield > 0) {
                let shieldperc = this.shield / this.originalShield;
                if (shieldperc > 0.6) {
                    fill(0, 153, 255);
                } else if (shieldperc <= 0.6 && shieldperc > 0.3) {
                    fill(255, 102, 0);
                } else {
                    fill(255, 0, 0);
                }
                noStroke();
                ellipse(0, 0, Tile.SIZE + Tile.SIZE * 0.15 * Math.random(), Tile.SIZE + Tile.SIZE * 0.15 * Math.random());

                fill(255, 255, 255, 180);
                ellipse(0, 0, Tile.SIZE * 0.9, Tile.SIZE * 0.9);
            }

            rotate(this.rotation);
            let imgw = Assets.get(this.sprite).width / this.cantimages;
            let img = Assets.get(this.sprite).get(imgw * (this.imgActual - 1), 0, imgw, 32);
            image(img, -this.w / 2, -this.h / 2, this.w, this.h);

            pop();
            push();
            fill(255, 0, 0);
            rect(this.x - 12, this.y - this.h / 2 - 18, 24, 5);
            fill(0, 255, 0);
            if (this.life > 0) {
                let lifeToDraw = Math.trunc((this.life * 24) / this.originalLife);
                rect(this.x - 12, this.y - this.h / 2 - 18, lifeToDraw, 5);
            }
            fill(255, 0, 0);
            rect(this.x - 12, this.y - this.h / 2 - 13, 24, 3);
            if (this.weapon.munition > 0) {
                fill('#0099ff');
                let munitionToDraw = Math.trunc((this.weapon.munition * 24) / this.weapon.originalMunition);
                rect(this.x - 12, this.y - this.h / 2 - 13, munitionToDraw, 3);
            } else {
                fill('white');
                let reloadToDraw = Math.trunc((this.weapon.reloadCounter * 24) / this.weapon.originalMunition);
                rect(this.x - 12, this.y - this.h / 2 - 13, reloadToDraw, 3);
            }
            pop();
        } else {
            if (!this.reappear) {
                push();
                translate(this.x, this.y);
                let imgw = Assets.get(this.spriteDestruction).width / this.cantimagesDestruction;
                let imgh = Assets.get(this.spriteDestruction).height;
                let img = Assets.get(this.spriteDestruction).get(imgw * (this.imgActual - 1), 0, imgw, imgh);
                image(img, (-this.w * 2) / 2, (-this.h * 3) / 2, this.w * 2, this.h * 2);
                pop();
            }
        }
    }
}
