class InputManager {
    static get Instance() {
        if (this._instance === undefined) {
            this._instance = new InputManager();
        }
        return this._instance;
    }

    constructor() {
        this.mapKeysPressed = {}; /*{UP: false, DOWN: false, LEFT: false, RIGHT: false, SHOOT: false};*/
        this.mapKeysReleased = {};
        for (const prop in Keys.Controls) {
            this.mapKeysPressed[prop] = false;
            this.mapKeysReleased[prop] = false;
        }

        let handleKeyPressed = function(e, mapkey, key_action) {
            e = e || window.event;
            for (const prop in Keys.Controls) {
                if (Keys.Controls[prop] === e.keyCode) {
                    mapkey[prop] = e.type === key_action;
                    break;
                }
            }
        };

        document.body.onkeydown = e => {
            handleKeyPressed(e, this.mapKeysPressed, 'keydown');
            NetworkManager.Instance.socket.emit('key_press', this.mapKeysPressed);
            if (e.keyCode === Keys.INFO) {
                Stage.Instance.InfPaneVisible = true;
            }
        };
        document.body.onkeyup = e => {
            handleKeyPressed(e, this.mapKeysPressed, 'keydown');
            // handleKeyPressed(e, this.mapKeysReleased, 'keyup');

            NetworkManager.Instance.socket.emit('key_release', this.mapKeysPressed);
            if (e.keyCode === Keys.INFO) {
                Stage.Instance.InfPaneVisible = false;
            }
            //for (const prop in Keys.Controls) {
            //   this.mapKeysReleased[prop] = false;
            //}
        };
    }
}

class Keys {
    static get LEFT() {
        return 65;
    }
    static get RIGHT() {
        return 68;
    }
    static get UP() {
        return 87;
    }
    static get DOWN() {
        return 83;
    }
    static get SHOOT() {
        return 90;
    }
    static get INFO() {
        return 32;
    }
    static get Controls() {
        return {LEFT: 37, RIGHT: 39, UP: 38, DOWN: 40, SHOOT: 90};
    }
}
