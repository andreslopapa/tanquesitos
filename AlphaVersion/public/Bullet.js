class Bullet {
    //constructor(sprite, x, y, w, h, speed, angle, damage, cantimages) {
    //  if (typeof sprite !== 'function') {
    //    Object.assign(this, {
    //      get sprite() {
    //        return Assets.get(sprite);
    //      },
    //      get spritex() {
    //        return -this.sprite.width / cantimages;
    //      },
    //      get img() {
    //        return this.sprite.get(0, 0, this.sprite.width / cantimages, this.sprite.height);
    //      },
    //    });
    //    this.cantimages = cantimages;
    //    this.rate = 0;
    //  } else {
    //    this.sprite = sprite;
    //  }
    //  this.x = x;
    //  this.y = y;
    //  this.speed = speed;
    //  this.w = w;
    //  this.h = h;
    //  this.angle = angle;
    //  this.damage = damage;
    //  this.xMove = speed * cos(angle);
    //  this.yMove = speed * sin(angle);
    //  this.deleteEntity = false;
    //}

    constructor(bulletClone) {
        this.getDataFromClone(bulletClone);
    }

    getDataFromClone(bulletClone) {
        for (const prop in bulletClone) {
            this[prop] = bulletClone[prop];
        }
    }

    render() {
        push();
        translate(this.x, this.y);
        rotate(this.angle);
        if (this.imageIsFunction) {
            let drawimage = Assets.get(this.sprite);
            drawimage(this.color);
        } else {
            let imgw = Assets.get(this.sprite).width / this.cantimages;
            let imgh = Assets.get(this.sprite).height;
            let img = Assets.get(this.sprite).get(imgw * (this.imgActual - 1), 0, imgw, imgh);
            image(img, 0, -this.h / 2, this.w, this.h);
        }
        pop();
    }

    crack(thecollidable) {
        let res = thecollidable.resistance / thecollidable.originalResistance;
        let crackImage = {};
        if (res < 0.66) {
            if (res < 0.66 && res >= 0.33) {
                crackImage = 'blockCrack1';
            } else if (res < 0.33) {
                crackImage = 'blockCrack2';
            }
            //  Map.Instance().blockChanges.push({
            //      type: 'crack',
            //      row: Math.trunc(thecollidable.posY / Tile.SIZE),
            //      col: Math.trunc(thecollidable.posX / Tile.SIZE),
            //      resistance:thecollidable.resistance,
            //      image: crackImage,
            //  });
        }
    }
    destroyBlock(thecollidable) {
        if (!(thecollidable.breakable && thecollidable.resistance <= 0)) {
            //Map.Instance().blockChanges.push({
            //    type: 'deleteEntity',
            //    row: Math.trunc(thecollidable.posY / Tile.SIZE),
            //    col: Math.trunc(thecollidable.posX / Tile.SIZE),
            //});
            //thecollidable.idImg = -1;
            let explosion = new Explosion(
                'brickSmoke',
                thecollidable.posX + Tile.SIZE / 2,
                thecollidable.posY + Tile.SIZE / 2,
                Tile.SIZE,
                Tile.SIZE,
                5,
                false,
            );
            //   Map.Instance().blockChanges.push({
            //       type: 'explosion',
            //       explosion: explosion,
            //       sound: false,
            //   });
            //   Stage.Instance.addEntity(explosion);
            this.explosions.push(explosion);
        } else {
            this.crack(thecollidable);
        }
    }

    destroyNearbyBlocks(centralBlock, callback) {
        if (this.damage > this.commonDamageLimit) {
            let e = Tile.SIZE * 1.0;
            //let d = Math.cos(Math.PI / 4) * e;
            let points = [
                {px: centralBlock.posX + e * 1.1, py: centralBlock.posY},
                {px: centralBlock.posX, py: centralBlock.posY + e * 1.1},
                {px: centralBlock.posX - e * 0.5, py: centralBlock.posY},
                {px: centralBlock.posX, py: centralBlock.posY - e * 0.5},
                {px: centralBlock.posX + e * 1.1, py: centralBlock.posY + e * 1.1},
                {px: centralBlock.posX - e * 0.5, py: centralBlock.posY + e * 1.1},
                {px: centralBlock.posX - e * 0.5, py: centralBlock.posY - e * 0.5},
                {px: centralBlock.posX + e * 1.1, py: centralBlock.posY - e * 0.5},
            ];
            for (let point of points) {
                if (point.px < 0) {
                    point.px = 0;
                } else if (point.px > Map.Instance().numCols * Tile.SIZE) {
                    point.px = Map.Instance().numCols * Tile.SIZE - 1;
                }
                if (point.py < 0) {
                    point.py = 0;
                } else if (point.py > Map.Instance().numRows * Tile.SIZE) {
                    point.py = Map.Instance().numRows * Tile.SIZE - 1;
                }
                let nearcolli = Map.Instance().getThereACollidable(point.px, point.py, 1, 1);
                if (nearcolli !== null) {
                    nearcolli.resistance -= this.damage * 0.7;
                    callback.call(this, nearcolli);
                }
            }
        }
    }

    destroyNearbyTanks(x, y) {
        if (this.damage > this.commonDamageLimit) {
            for (let t of Map.Instance().entities['Tank']) {
                let distance = Math.sqrt(Math.pow(t.x - x, 2) + Math.pow(t.y - y, 2));
                if (distance < 1.5 * Tile.SIZE) {
                    t.destroy(this.damage * 0.7);
                    if (t.life < 0 && t.id !== this.idTank) {
                        this.tanksDestroyed++;
                    }
                }
            }
        }
    }
    update() {
        this.rate += 1;
        if (this.rate > 5) {
            this.rate = 0;
            this.imgActual += 1;
            if (this.imgActual > this.cantimages) {
                this.imgActual = 1;
            }
        }
        let colli = null;
        let tank = null;
        let distancia = Math.sqrt(Math.pow(this.xMove, 2) + Math.pow(this.yMove, 2));
        for (let i = 0; i < distancia; i += 3) {
            let addExplosion = (xAux, yAux) => {
                this.xMove = i * Math.cos(this.angle);
                this.yMove = i * Math.sin(this.angle);
                this.deleteEntity = true;
                //let explX = this.damage > 300 ? xAux : this.x + this.xMove;
                //let explY = this.damage > 300 ? yAux : this.y + this.yMove;
                //let explosion = new Explosion('explosionimg', explX, explY, this.damage, this.damage, 6, true);
                //this.explosions.push(explosion);
                //SoundManager.Instance.stopSound('explosion');
                //SoundManager.Instance.playSound('explosion', false);
            };
            let xEvaluated = this.x + i * Math.cos(this.angle);
            let yEvaluated = this.y + i * Math.sin(this.angle);
            colli = Map.Instance().getThereACollidable(xEvaluated, yEvaluated, this.w, this.h);
            if (colli !== null) {
                colli.resistance -= this.damage;
                //this.destroyNearbyTanks(colli.posX + Tile.SIZE / 2, colli.posY + Tile.SIZE / 2);
                //this.destroyNearbyBlocks(colli, this.destroyBlock);
                //this.destroyBlock(colli);
                //addExplosion(colli.posX + Tile.SIZE / 2, colli.posY + Tile.SIZE / 2);
                this.xMove = i * Math.cos(this.angle);
                this.yMove = i * Math.sin(this.angle);
                this.deleteEntity = true;
                break;
            } else {
                tank = Map.Instance().getATank(xEvaluated, yEvaluated, this.w, this.h, this.idTank);
                if (tank !== null) {
                    tank.destroy(this.damage * 0.7);
                    this.xMove = i * Math.cos(this.angle);
                    this.yMove = i * Math.sin(this.angle);
                    this.deleteEntity = true;
                    break;
                    //addExplosion(tank.x, tank.y);
                    //this.destroyNearbyTanks(tank.x, tank.y);
                    //let blockOfTank = {
                    //    posX: Math.trunc(tank.x / Tile.SIZE) * Tile.SIZE,
                    //    posY: Math.trunc(tank.y / Tile.SIZE) * Tile.SIZE,
                    //};
                    //this.destroyNearbyBlocks(blockOfTank, this.destroyBlock);
                    //if (tank.life < 0) {
                    //    this.tanksDestroyed++;
                    //    SoundManager.Instance.stopSound('explosion');
                    //    SoundManager.Instance.stopSound('tankExplosion');
                    //    SoundManager.Instance.playSound('tankExplosion', false);
                    //}
                }
            }
        }
        /* colli = Map.Instance().getThereACollidable(xEvaluated, yEvaluated, this.w , this.h );*/
        /*if (colli === null) {*/

        this.x += this.xMove;
        this.y += this.yMove;
        /*}else{
	    this.exploted=true;
	}*/
    }
}
