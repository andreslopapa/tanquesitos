class Map {
    static Instance(mapJSON, mapClone) {
        if (this._instance === undefined) {
            this._instance = new Map(mapJSON, mapClone);
        }
        return this._instance;
    }

    //constructor(map, entities) {
    //    this.entities = entities;
    //    this.hasBeenResized = true;
    //    this.scl = 1;
    //    this.layers = map.layers;
    //    this.numRows = map.height;
    //    this.numCols = map.width;
    //    this.loadSpritesTiles(map);
    //    this.backgroundImage = createImage(Math.floor(Map.numCols * Tile.SIZE), Math.floor(Map.numRows * Tile.SIZE));
    //    this.map3DTiles = [];
    //    for (let i = 0; i < this.numRows; ++i) {
    //        this.map3DTiles[i] = [];
    //        for (let j = 0; j < this.numCols; ++j) {
    //            this.map3DTiles[i][j] = [];
    //        }
    //    }
    //    this.collidables = map.layers[0].objects;
    //    for (let l = 0; l < this.layers.length; ++l) {
    //        if (this.layers[l].type === 'objectgroup') {
    //            continue;
    //        }
    //        for (let r = 0; r < this.numRows; ++r) {
    //            for (let c = 0; c < this.numCols; ++c) {
    //                let index = c + r * this.numCols;
    //                let idImg = this.layers[l].data[index] - 2;
    //                let x = c * Tile.SIZE;
    //                let y = r * Tile.SIZE;
    //                let breakable = true;
    //                let resistance = 0;
    //                if (this.layers[l].name === 'bloques') {
    //                    let collidable = this.collidables.filter(c => c.x === x && c.y === y);
    //                    if (collidable[0] !== undefined) {
    //                        breakable = !collidable[0].properties.unbreakable;
    //                        resistance = collidable[0].properties.resistance;
    //                    }
    //                }

    //                this.map3DTiles[r][c][l] = new Tile(idImg, x, y, breakable, resistance);
    //            }
    //        }
    //    }
    //}

    constructor(mapJSON, mapClone) {
        Object.assign(this, mapClone);
        this.loadSpritesTiles(mapJSON);
        this.backgroundImage = createImage(Math.floor(mapJSON.width * Tile.SIZE), Math.floor(mapJSON.height * Tile.SIZE));
    }

    loadSpritesTiles(map) {
        //for (let i = 0; i < map.tilesets.length; ++i) {
        //    let sprImage = Assets.get(map.tilesets[i].source.slice(0, map.tilesets[i].source.length - 4));
        //    for (let y = 0; y < sprImage.height; y += Tile.SIZE) {
        //        for (let x = 0; x < sprImage.width; x += Tile.SIZE) {
        //            TileManager.registerTileImage(sprImage.get(x, y, Tile.SIZE, Tile.SIZE));
        //        }
        //    }
        //}
        let sprites = [];
        let limInf = 1;
        let limSup;
        for (let i = 0; i < map.tilesets.length; ++i) {
            let sprImage = Assets.get(map.tilesets[i].source.slice(0, map.tilesets[i].source.length - 4));
            let tilesXRow = sprImage.width / Tile.SIZE;
            let cantTiles = (sprImage.width / Tile.SIZE) * (sprImage.height / Tile.SIZE);
            limSup = limInf + cantTiles - 1;
            sprites.push({sprImage: sprImage, limInf: limInf, limSup: limSup, tilesXRow: tilesXRow});
            limInf += cantTiles;
        }
        for (let l = 0; l < this.layers.length; ++l) {
            if (this.layers[l].type === 'objectgroup') {
                continue;
            }
            for (let r = 0; r < this.numRows; ++r) {
                for (let c = 0; c < this.numCols; ++c) {
                    let index = c + r * this.numCols;
                    let idImg = this.layers[l].data[index];
                    if (idImg > 0) {
                        for (let spts of sprites) {
                            if (idImg >= spts.limInf && idImg <= spts.limSup) {
                                let yTileImg = Math.trunc((idImg - spts.limInf) / spts.tilesXRow);
                                let xTileImg = idImg - yTileImg * spts.tilesXRow - spts.limInf;
                                yTileImg *= Tile.SIZE;
                                xTileImg *= Tile.SIZE;
                                let tileImg = spts.sprImage.get(xTileImg, yTileImg, Tile.SIZE, Tile.SIZE);
                                TileManager.registerTileImage(idImg, tileImg);
                                break;
                            }
                        }
                    }
                }
            }
        }
        for (let i = 0; i < map.tilesets.length; ++i) {
            delete Assets._assets[map.tilesets[i].source.slice(0, map.tilesets[i].source.length - 4)];
        }
    }

    getThereACollidable(x, y, w, h) {
        let collidableC = posX => {
            return Math.trunc(posX / Tile.SIZE);
        };
        let collidableR = posY => {
            return Math.trunc(posY / Tile.SIZE);
        };

        // let index = c + r * (this.numCols);
        let c;
        let points = [
            {px: x, py: y - h / 2},
            {px: x, py: y + h / 2},
            {px: x - w / 2, py: y - h / 2},
            {px: x + w / 2, py: y - h / 2},
            {px: x - w / 2, py: y + h / 2},
            {px: x + w / 2, py: y + h / 2},
            {px: x - w / 2, py: y},
            {px: x + w / 2, py: y},
        ];
        for (let l = 0; l < this.map3DTiles[0][0].length; ++l) {
            for (let i = 0; i < points.length; ++i) {
                c = this.map3DTiles[collidableR(points[i].py)][collidableC(points[i].px)][l];
                if (c !== undefined && c !== null && !(c.breakable && c.resistance <= 0)) {
                    return c;
                }
            }
        }
        return null;
    }

    getAPowerUp(x, y, w, h) {
        let getColumna = posX => {
            return Math.trunc(posX / Tile.SIZE);
        };
        let getFila = posY => {
            return Math.trunc(posY / Tile.SIZE);
        };
        let points = [
            {px: x - w / 2, py: y - h / 2},
            {px: x + w / 2, py: y - h / 2},
            {px: x - w / 2, py: y + h / 2},
            {px: x + w / 2, py: y + h / 2},
            {px: x - w / 2, py: y},
            {px: x + w / 2, py: y},
            {px: x, py: y - h / 2},
            {px: x, py: y + h / 2},
        ];
        for (let p of this.entities['PowerUp']) {
            for (let i = 0; i < points.length; ++i) {
                if (getColumna(p.x) === getColumna(points[i].px) && getFila(p.y) === getFila(points[i].py)) {
                    return p;
                }
            }
        }
        return null;
    }

    getATank(x, y, w, h, idTankToAvoid) {
        let points = [
            {px: x, py: y - h / 2},
            {px: x, py: y + h / 2},
            {px: x - w / 2, py: y - h / 2},
            {px: x + w / 2, py: y - h / 2},
            {px: x - w / 2, py: y + h / 2},
            {px: x + w / 2, py: y + h / 2},
            {px: x - w / 2, py: y},
            {px: x + w / 2, py: y},
        ];
        for (let t of this.entities['Tank']) {
            if ((idTankToAvoid !== undefined && t.id === idTankToAvoid) || t.destroyed) {
                continue;
            }
            for (let i = 0; i < points.length; ++i) {
                let tll = t.x - t.w / 2;
                let tlr = t.x + t.w / 2;
                let tlb = t.y + t.h / 2;
                let tlt = t.y - t.h / 2;
                let condition = points[i].px > tll && points[i].px < tlr && points[i].py < tlb && points[i].py > tlt;
                if (condition) {
                    return t;
                }
            }
        }
        return null;
    }
    onResize() {
        if (height < width) {
            this.scl = width / (this.numRows * Tile.SIZE);
        } else {
            this.scl = height / (this.numCols * Tile.SIZE);
        }
        if (this.scl > 1) {
            this.scl = 1;
        } else if (this.scl < 1) {
            this.scl *= 0.9;
        }
        this.hasBeenResized = true;
    }

    renderBlockChanges(blockChanges) {
        for (let change of blockChanges) {
            if (change.type === 'crack') {
                let idimg = Map.Instance().map3DTiles[change.row][change.col][2].idImg;
                Map.Instance().map3DTiles[change.row][change.col][2].resistance = change.resistance;
                if (idimg >= 0) {
                    let crackImage = Assets.get(change.image);
                    this.backgroundImage.blend(
                        crackImage,
                        0,
                        0,
                        crackImage.width,
                        crackImage.height,
                        change.col * Tile.SIZE,
                        change.row * Tile.SIZE,
                        Tile.SIZE,
                        Tile.SIZE,
                        BLEND,
                    );
                }
            } else if (change.type === 'deleteEntity') {
                let idimg = Map.Instance().map3DTiles[change.row][change.col][2].idImg;
                Map.Instance().map3DTiles[change.row][change.col][2].resistance = 0;
                if (idimg >= 0) {
                    Map.Instance().map3DTiles[change.row][change.col][2].idImg = -1;
                    let idimgfondo = Map.Instance().map3DTiles[change.row][change.col][1].idImg;
                    if (idimgfondo > 0) {
                        this.backgroundImage.blend(
                            TileManager.getTileImage(idimgfondo),
                            0,
                            0,
                            Tile.SIZE,
                            Tile.SIZE,
                            change.col * Tile.SIZE,
                            change.row * Tile.SIZE,
                            Tile.SIZE,
                            Tile.SIZE,
                            BLEND,
                        );
                    }
                }
            }
        }
    }

    render(camera) {
        this.xo = 0;
        if (height < width) {
            this.xo = (width - this.numCols * this.scl * Tile.SIZE) / 2;
        }
        if (Stage.Instance.mainPlayer !== undefined) {
            Stage.Instance.camera.followEntity(Stage.Instance.mainPlayer);
        }
        translate(this.xo, 0);
        this.transx = -camera.xOffset === -0 ? 0 : -camera.xOffset;
        this.transy = -camera.yOffset === -0 ? 0 : -camera.yOffset;

        translate(this.transx, this.transy);

        if (this.hasBeenResized) {
            this.hasBeenResized = false;

            for (let l = 0; l < this.layers.length; ++l) {
                if (this.layers[l].type === 'objectgroup') {
                    continue;
                }
                for (let r = 0; r < this.numRows; ++r) {
                    for (let c = 0; c < this.numCols; ++c) {
                        //let index = c + r * (this.numCols);
                        //let idImg = this.layers[l].data[index] - 2;
                        let tile = this.map3DTiles[r][c][l];
                        let idImg = tile.idImg;
                        if (idImg <= 0) {
                            continue;
                        }

                        this.backgroundImage.blend(
                            TileManager.getTileImage(idImg),
                            0,
                            0,
                            Tile.SIZE,
                            Tile.SIZE,
                            c * Tile.SIZE,
                            r * Tile.SIZE,
                            Tile.SIZE,
                            Tile.SIZE,
                            BLEND,
                        );
                        if (tile.resistance) {
                            let res = tile.resistance / tile.originalResistance;
                            let crackImage = {};
                            if (res < 0.66) {
                                if (res < 0.66 && res >= 0.33) {
                                    crackImage = 'blockCrack1';
                                } else if (res < 0.33) {
                                    crackImage = 'blockCrack2';
                                }
                                this.backgroundImage.blend(
                                    Assets.get(crackImage),
                                    0,
                                    0,
                                    Tile.SIZE,
                                    Tile.SIZE,
                                    c * Tile.SIZE,
                                    r * Tile.SIZE,
                                    Tile.SIZE,
                                    Tile.SIZE,
                                    BLEND,
                                );
                            }
                        }
                    }
                }
            }
        }

        image(
            this.backgroundImage,
            0,
            0,
            Math.floor(this.numCols * Tile.SIZE * this.scl),
            Math.floor(this.numCols * Tile.SIZE * this.scl),
        );
        scale(this.scl);
    }
}
