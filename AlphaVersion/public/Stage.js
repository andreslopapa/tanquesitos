class Stage {
    constructor() {
        this.entities = {Bullet: [], Explosion: [], PowerUp: [], Tank: []};
        this.cnv = {};
        this.InfPaneVisible = false;
        this.pingRequested = true;
    }

    static get Instance() {
        if (this._instance === undefined) {
            this._instance = new Stage();
        }
        return this._instance;
    }

    addEntity(e) {
        let index = this.entities[e.constructorName].length;
        this.entities[e.constructorName][index] = e;
    }

    removeEntity(e) {
        let index = this.entities[e.constructorName].indexOf(e);
        this.entities[e.constructorName].splice(index, 1);
    }

    addMainPlayer(p) {
        this.mainPlayer = p;
    }

    preloadAssets() {
        Assets.initialize();
        let promises = [];
        let addPromise = function(nameAudio, pathAudio) {
            return new Promise(function(resolve, reject) {
                Assets.registerAsset(nameAudio, Assets.loadASound(pathAudio));
                Assets.get(nameAudio).on('load', function() {
                    resolve();
                });
                Assets.get(nameAudio).on('loaderror', function(id, error) {
                    reject('audio:' + id + ' error:' + error);
                });
            });
        };
        //let prom1 = new Promise(function(resolve, reject) {
        //    Assets.registerAsset('shot', Assets.loadASound('sounds/shot.mp3'));
        //    Assets.get('shot').on('load', function() {
        //        resolve();
        //    });
        //    Assets.get('shot').on('loaderror', function(id, error) {
        //        reject('audio:' + id + ' error:' + error);
        //    });
        //});
        //let prom2 = new Promise(function(resolve, reject) {
        //    Assets.registerAsset('missilelaunched', Assets.loadASound('sounds/missilelaunched.mp3'));
        //    Assets.get('missilelaunched').on('load', function() {
        //        resolve();
        //    });
        //});

        //prom1
        //    .then(function() {
        //        console.log('asd');
        //        return prom2;
        //    })
        //    .then(function() {
        //        console.log('holsi perruco');
        //    })
        //    .catch(reason => {
        //        console.log(reason);
        //    });

        promises.push(addPromise('shot', 'sounds/shot.mp3'));
        promises.push(addPromise('missilelaunched', 'sounds/missilelaunched.mp3'));
        promises.push(addPromise('movetank', 'sounds/movetank.mp3'));
        promises.push(addPromise('explosionsnd', 'sounds/explosion.mp3'));
        promises.push(addPromise('tankExplosion', 'sounds/tankexplosion.mp3'));
        promises.push(addPromise('HeavyMachineGunsnd', 'sounds/heavymachinegun.mp3'));
        promises.push(addPromise('RocketLaunchersnd', 'sounds/rocketlauncher.mp3'));
        promises.push(addPromise('SuperGrenadesnd', 'sounds/supergrenade.mp3'));
        promises.push(addPromise('Lifesnd', 'sounds/okay.mp3'));
        promises.push(addPromise('Shieldsnd', 'sounds/okay.mp3'));
        promises.push(addPromise('noBullets', 'sounds/nobullets.mp3'));
        promises.push(addPromise('engine', 'sounds/engine.mp3'));
        //promises.push(addPromise('sound1', 'sounds/sound1.mp3'));
        //promises.push(addPromise('sound2', 'sounds/sound2.mp3'));
        //promises.push(addPromise('sound3', 'sounds/sound3.mp3'));
        //promises.push(addPromise('sound4', 'sounds/sound4.mp3'));
        //promises.push(addPromise('sound5', 'sounds/sound5.mp3'));
        //promises.push(addPromise('sound6', 'sounds/sound6.mp3'));
        let preloadAllSoundsSync = async function() {
            await Promise.all(promises);
        };
        preloadAllSoundsSync();

        //        Assets.registerAsset('missilelaunched', Assets.loadASound('sounds/missilelaunched.mp3'));
        //        Assets.registerAsset('shot', Assets.loadASound('sounds/shot.mp3'));
        //        Assets.registerAsset('movetank', Assets.loadASound('sounds/movetank.mp3'));
        //        Assets.registerAsset('explosionsnd', Assets.loadASound('sounds/explosion.mp3'));
        //        Assets.registerAsset('tankExplosion', Assets.loadASound('sounds/tankexplosion.mp3'));
        //        Assets.registerAsset('HeavyMachineGunsnd', Assets.loadASound('sounds/heavymachinegun.mp3'));
        //        Assets.registerAsset('RocketLaunchersnd', Assets.loadASound('sounds/rocketlauncher.mp3'));
        //        Assets.registerAsset('SuperGrenadesnd', Assets.loadASound('sounds/supergrenade.mp3'));
        //        Assets.registerAsset('Lifesnd', Assets.loadASound('sounds/life.mp3'));
        //        Assets.registerAsset('noBullets', Assets.loadASound('sounds/nobullets.mp3'));
        //        Assets.registerAsset('engine', Assets.loadASound('sounds/engine.mp3'));
        //        Assets.registerAsset('sound1', Assets.loadASound('sounds/sound1.mp3'));
        //        Assets.registerAsset('sound2', Assets.loadASound('sounds/sound2.mp3'));
        //        Assets.registerAsset('sound3', Assets.loadASound('sounds/sound3.mp3'));
        //        Assets.registerAsset('sound4', Assets.loadASound('sounds/sound4.mp3'));
        //        Assets.registerAsset('sound5', Assets.loadASound('sounds/sound5.mp3'));
        //        Assets.registerAsset('sound6', Assets.loadASound('sounds/sound6.mp3'));

        Assets.registerAsset('redtank', loadImage('images/redtank.png'));
        Assets.registerAsset('bluetank', loadImage('images/bluetank.png'));
        Assets.registerAsset('greentank', loadImage('images/greentank.png'));
        Assets.registerAsset('pinktank', loadImage('images/pinktank.png'));
        Assets.registerAsset('mushroomexplosion', loadImage('images/mushroomexplosion.png'));
        Assets.registerAsset('explosionimg', loadImage('images/explosion.png'));
        Assets.registerAsset('shootSuperGrenade', function(colorB) {
            stroke(0, 0, 0);
            strokeWeight(6);
            line(0, 0, 6, 0);
            strokeWeight(2);
            line(8, 0, 9, 0);
            stroke(colorB);
            strokeWeight(2);
            line(-3, 0, 1, 0);
            line(-3, 4.5, 1, 3);
            line(-3, -4.5, 1, -3);
        });
        Assets.registerAsset('ammoIcon', loadImage('images/ammoicon.png'));
        Assets.registerAsset('misiles', loadImage('images/misiles.png'));
        Assets.registerAsset('shootMachineGun', function(colorB) {
            stroke(colorB);
            strokeWeight(2);
            line(0, 0, 4, 0);
        });
        Assets.registerAsset('HeavyMachineGunimg', loadImage('images/puminigun.png'));
        Assets.registerAsset('RocketLauncherimg', loadImage('images/purocket.png'));
        Assets.registerAsset('SuperGrenadeimg', loadImage('images/pusupergrenade.png'));
        Assets.registerAsset('Lifeimg', loadImage('images/pulife.png'));
        Assets.registerAsset('Shieldimg', loadImage('images/pushield.png'));
        //Assets.registerAsset("map", Stage.Instance.loadJSONFile("./maps/mapa1.json"));
        //   Stage.Instance.loadJSONFile('./maps/mapa1.json', function(response) {
        //       Assets.registerAsset('map', JSON.parse(response));
        //   });
        Assets.registerAsset('tileset', loadImage('maps/tileset.png'));
        Assets.registerAsset('bgTiles', loadImage('maps/bgTiles.png'));
        Assets.registerAsset('blockTiles', loadImage('maps/blockTiles.png'));
        Assets.registerAsset('blockCrack1', loadImage('images/blockcrack1.png'));
        Assets.registerAsset('blockCrack2', loadImage('images/blockcrack2.png'));

        Assets.registerAsset('brickSmoke', loadImage('images/bricksmoke.png'));
        Assets.registerAsset('shootSmoke', loadImage('images/shootsmoke.png'));
    }

    loadJSONFile(path, callback) {
        var xobj = new XMLHttpRequest();
        xobj.overrideMimeType('application/json');
        xobj.open('GET', path, true);
        xobj.onreadystatechange = function() {
            if (xobj.readyState == 4 && xobj.status == '200') {
                // Required use of an anonymous callback as .open will NOT return a value but simply returns undefined in asynchronous mode
                callback(xobj.responseText);
            }
        };
        xobj.send(null);
    }

    initialize() {
        this.cnv = createCanvas(displayWidth, displayHeight);
        /* let x = (windowWidth - width) / 2;
	 let y =(windowHeight-height)/2;*/
        this.cnv.position(0, 0);
        width = windowWidth;
        height = windowHeight;

        //SoundManager.Instance.registerSounds('sound1', Assets.get('sound1'), SoundManager.TYPESOUND.MUSIC);
        //SoundManager.Instance.registerSounds('sound2', Assets.get('sound2'), SoundManager.TYPESOUND.MUSIC);
        //SoundManager.Instance.registerSounds('sound3', Assets.get('sound3'), SoundManager.TYPESOUND.MUSIC);
        //SoundManager.Instance.registerSounds('sound4', Assets.get('sound4'), SoundManager.TYPESOUND.MUSIC);
        //SoundManager.Instance.registerSounds('sound5', Assets.get('sound5'), SoundManager.TYPESOUND.MUSIC);
        //SoundManager.Instance.registerSounds('sound6', Assets.get('sound6'), SoundManager.TYPESOUND.MUSIC);
        SoundManager.Instance.registerSounds('engine', Assets.get('engine'), SoundManager.TYPESOUND.EFFECT);
        SoundManager.Instance.registerSounds('movetank', Assets.get('movetank'), SoundManager.TYPESOUND.EFFECT);
        SoundManager.Instance.registerSounds('shot', Assets.get('shot'), SoundManager.TYPESOUND.EFFECT);
        SoundManager.Instance.registerSounds('missilelaunched', Assets.get('missilelaunched'), SoundManager.TYPESOUND.EFFECT);
        SoundManager.Instance.registerSounds('explosion', Assets.get('explosionsnd'), SoundManager.TYPESOUND.EFFECT);
        SoundManager.Instance.registerSounds('tankExplosion', Assets.get('tankExplosion'), SoundManager.TYPESOUND.EFFECT);
        SoundManager.Instance.registerSounds('HeavyMachineGun', Assets.get('HeavyMachineGunsnd'), SoundManager.TYPESOUND.EFFECT);
        SoundManager.Instance.registerSounds('RocketLauncher', Assets.get('RocketLaunchersnd'), SoundManager.TYPESOUND.EFFECT);
        SoundManager.Instance.registerSounds('SuperGrenade', Assets.get('SuperGrenadesnd'), SoundManager.TYPESOUND.EFFECT);
        SoundManager.Instance.registerSounds('Life', Assets.get('Lifesnd'), SoundManager.TYPESOUND.EFFECT);
        SoundManager.Instance.registerSounds('Shield', Assets.get('Shieldsnd'), SoundManager.TYPESOUND.EFFECT);
        SoundManager.Instance.registerSounds('noBullets', Assets.get('noBullets'), SoundManager.TYPESOUND.EFFECT);
        SoundManager.Instance.playSound('engine', true);

        //    Map.Instance(Assets.get('map'), this.entities);
        this.camera = new Camera();
        //		Stage.Instance.addEntity(new PowerUp(PowerUp.Type.WEAPON.HeavyMachineGun, 12 * Tile.SIZE, 12 * Tile.SIZE, Tile.SIZE, Tile.SIZE));

        if (this.Map !== undefined) {
            this.Map.onResize();
        }
    }

    //	handleKeysReleased() {
    //		if (this.mainPlayer !== undefined) {
    //			this.mainPlayer.handleKeysReleased();
    //		}
    //	}

    render() {
        background(0);
        push();
        if (this.Map !== undefined) {
            this.Map.render(this.camera);
        }
        for (let group in this.entities) {
            if (group === 'Bullet') {
                continue;
            }
            for (let entity of this.entities[group]) {
                if (entity === this.mainPlayer) {
                    continue;
                }
                entity.render();
            }
        }
        //render mainPlayer on top of other players
        if (this.mainPlayer !== undefined) {
            this.mainPlayer.playSounds();
            this.mainPlayer.render();
        }

        //render bullets on top
        for (let bullet of this.entities['Bullet']) {
            bullet.render();
        }
        textSize(18);
        text(floor(frameRate()), 60, 60);
        push();
        if (this.InfPaneVisible) {
            fill(0, 0, 0, 180);
            let widthtab;
            if (Map.Instance().numCols * Tile.SIZE > width) {
                widthtab = width;
            } else {
                widthtab = Map.Instance().numCols * Tile.SIZE;
            }
            widthtab = (widthtab * 0.56) / Map.Instance().scl;
            let heighttab = (height * 0.7) / Map.Instance().scl;
            stroke('red');
            strokeWeight(2);
            let xPaneOrig = -Map.Instance().xo - Map.Instance().transx + width / Map.Instance().scl / 2 - widthtab / 2;
            let yPaneOrig = -Map.Instance().transy + height / Map.Instance().scl / 2 - heighttab / 2;
            rect(xPaneOrig, yPaneOrig, widthtab, heighttab, 10);
            translate(xPaneOrig, yPaneOrig);
            textAlign(CENTER, CENTER);
            textFont('Courier New');
            textStyle(BOLD);
            textSize(19);
            noStroke();
            fill('white');
            text('Tanquesitos', widthtab / 2, 20);
            textSize(14);
            fill('#e0e0d1');
            text('Tank', widthtab / 5, 50);
            text('Kills', (widthtab * 2) / 5, 50);
            text('Deaths', (widthtab * 3) / 5, 50);
            text('Ping', (widthtab * 4) / 5, 50);
            let hposition = 50;
            for (let tank of this.entities['Tank']) {
                hposition += 40;
                fill(tank.colorTank);
                let imgwt = Assets.get(tank.sprite).width / tank.cantimages;
                let imght = Assets.get(tank.sprite).height;
                let imgt = Assets.get(tank.sprite).get(0, 0, imgwt, imght);
                image(imgt, widthtab / 5 - imgwt / 2, hposition - imght / 2, imgwt, imght);
                text(tank.kills, (widthtab * 2) / 5, hposition);
                text(tank.deaths, (widthtab * 3) / 5, hposition);
                let pingToShow = '';
                if (tank.ping < 1000) {
                    pingToShow = tank.ping + ' ms';
                } else if (tank.ping >= 1000 && tank.ping < 10000) {
                    pingToShow = tank.ping + ' s';
                } else {
                    pingToShow = 'Inf';
                }
                text(pingToShow, (widthtab * 4) / 5, hposition);
            }
        }
        pop();
        push();
        if (this.Map !== undefined && this.mainPlayer != undefined) {
            let widthtabBottom = Tile.SIZE * 10;
            let heighttabBottom = Tile.SIZE * 1;
            let xBottomPane = -Map.Instance().xo - Map.Instance().transx + width / Map.Instance().scl / 2 - widthtabBottom / 2;
            let yBottomPane;
            if (height < Map.Instance().numRows * Tile.SIZE * Map.Instance().scl) {
                yBottomPane = -Map.Instance().transy + (height / Map.Instance().scl - 0.5 * Tile.SIZE) - heighttabBottom / 2;
            } else {
                yBottomPane = -Map.Instance().transy + (Map.Instance().numRows - 0.5) * Tile.SIZE - heighttabBottom / 2;
            }
            fill(0, 0, 0, 180);
            stroke('#003399');
            strokeWeight(2);
            rect(xBottomPane, yBottomPane, widthtabBottom, heighttabBottom, 4);
            translate(xBottomPane, yBottomPane);
            noStroke();
            textAlign(LEFT, CENTER);
            textFont('Courier New');
            textStyle(BOLD);
            textSize(23);
            let lifeToShow = this.mainPlayer.life < 0 ? 0 : this.mainPlayer.life;
            if (this.mainPlayer.shield > 0) {
                lifeToShow += this.mainPlayer.shield;
                fill('#606368');
            } else {
                fill('#009900');
            }
            if (this.mainPlayer.life < this.mainPlayer.originalLife * 0.2) {
                fill('red');
            }
            rect(
                widthtabBottom / 6 - (heighttabBottom * 0.8) / 2,
                heighttabBottom / 2 - (heighttabBottom * 0.3) / 2,
                heighttabBottom * 0.8,
                heighttabBottom * 0.3,
                3,
            );
            rect(
                widthtabBottom / 6 - (heighttabBottom * 0.3) / 2,
                heighttabBottom / 2 - (heighttabBottom * 0.8) / 2,
                heighttabBottom * 0.3,
                heighttabBottom * 0.8,
                3,
            );
            text(lifeToShow, widthtabBottom / 6 + heighttabBottom / 2, (heighttabBottom * 1.1) / 2);

            fill(0, 0, 0, 200);
            stroke('#cc0000');
            ellipse((widthtabBottom * 3) / 6, heighttabBottom / 2, heighttabBottom * 0.9, heighttabBottom * 0.9);
            noStroke();
            textAlign(CENTER, CENTER);
            textSize(25);
            fill('#f5f5f0');
            text(this.mainPlayer.weapon.letter, (widthtabBottom * 3) / 6, (heighttabBottom * 1.1) / 2);

            textSize(23);
            textAlign(LEFT, CENTER);
            let imgAmmoIcon = Assets.get('ammoIcon');
            image(
                imgAmmoIcon,
                (widthtabBottom * 4) / 6 - (heighttabBottom * 0.8) / 2,
                heighttabBottom * (1 / 2 - 0.4),
                heighttabBottom * 0.8,
                heighttabBottom * 0.8,
            );
            fill('#e6b800');
            let ammoToShow = this.mainPlayer.weapon.munition;
            if (ammoToShow === undefined || ammoToShow < 0) {
                ammoToShow = 0;
            }
            text(' x ' + ammoToShow, (widthtabBottom * 4) / 6 + (heighttabBottom * 0.9) / 2, (heighttabBottom * 1.1) / 2);
        }
        pop();
        pop();
    }

    update() {
        if (this.pingRequested && this.mainPlayer !== undefined) {
            this.pingRequested = false;
            NetworkManager.Instance.socket.emit('pingRequested', {timeClient: Date.now(), pingMainPlayer: this.mainPlayer.ping});
            setTimeout(() => {
                this.pingRequested = true;
            }, 1000);
        }
        for (let group in this.entities) {
            for (let entity of this.entities[group]) {
                entity.update();
            }
        }
    }
}
