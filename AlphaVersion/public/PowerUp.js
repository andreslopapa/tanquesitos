class PowerUp {
    //constructor(type, x, y, w, h) {
    //  if (PowerUp.Type.WEAPON.hasOwnProperty(type)) {
    //    this.getPower = function() {
    //      this.weapon = new Weapon(type);
    //      SoundManager.Instance.stopSound(type);
    //      SoundManager.Instance.playSound(type, false);
    //    };
    //    this.img = Assets.get(type + 'img');
    //  } else {
    //    switch (type) {
    //      case PowerUp.Type.LIFE:
    //        break;
    //    }
    //  }
    //  this.x = x;
    //  this.y = y;
    //  this.w = w;
    //  this.h = h;
    //  this.deleteEntity = false;
    //}

    constructor(powerupClone) {
        this.getDataFromClone(powerupClone);
    }

    getDataFromClone(powerupClone) {
        Object.assign(this, powerupClone);
        this.getPower = function() {
            this.weapon = new Weapon(this.type);
        };
    }

    render() {
        push();
        translate(this.x, this.y);
        image(Assets.get(this.img), 0, 0, this.w, this.h);
        pop();
    }
    update() {}
}
PowerUp.Type = {
    WEAPON: Weapon.Type,
    LIFE: 'Life',
    SHIELD:'Shiel',
};
