let cnv;
let camera;

function preload() {
    //let tuvieja = function() {
    //    setTimeout(() => {
    //        console.log('tu vieja');
    //    }, 4000);
    //};
    //registerPreloadMethod('tuvieja', p5.prototype);
    //let prom = new Promise((resolve, reject) => {
    //    setTimeout(resolve(), 3000);
    //});
    //let a =async function() {
    //    await prom.then(function() {
    //        console.log('tu mama');
    //    });
    //};
    //a();
    //while (soundsNoLoaded.wait) {}
    Stage.Instance.preloadAssets();
}

function setup() {
    frameRate(60);
    Stage.Instance.initialize();

    NetworkManager.Instance.addEventListener('current_map_entities', function(data) {
        let entities = data.map.entities;
        if (entities !== undefined) {
            for (let group in entities) {
                for (let e of entities[group]) {
                    switch (e.constructorName) {
                        case 'Tank':
                            Stage.Instance.addEntity(new Tank(e));
                            break;
                        case 'Explosion':
                            if (e.sound) {
                                SoundManager.Instance.stopSound('explosion');
                                SoundManager.Instance.playSound('explosion', false);
                            }
                            Stage.Instance.addEntity(new Explosion(e));
                            break;
                        case 'Bullet':
                            //SoundManager.Instance.stopSound(e.soundShot);
                            //SoundManager.Instance.playSound(e.soundShot, false);
                            Stage.Instance.addEntity(new Bullet(e));
                            break;
                        case 'PowerUp':
                            Stage.Instance.addEntity(new PowerUp(e));
                            break;
                    }
                }
            }
        }
        data.map.entities = Stage.Instance.entities;
        Stage.Instance.Map = Map.Instance(data.mapJSON, data.map);
    });

    NetworkManager.Instance.addEventListener('player_leave', function(playerIndex) {
        let entity = Stage.Instance.entities['Tank'][playerIndex];
        Stage.Instance.removeEntity(entity);
    });

    NetworkManager.Instance.addEventListener('entity_removed', function(data) {
        let entity = Stage.Instance.entities[data.group][data.index];
        if (entity instanceof PowerUp) {
            SoundManager.Instance.stopSound(entity.type);
            SoundManager.Instance.playSound(entity.type, false);
        }
        Stage.Instance.removeEntity(entity);
    });

    NetworkManager.Instance.addEventListener('entity_added', function(e) {
        switch (e.constructorName) {
            case 'Tank':
                Stage.Instance.addEntity(new Tank(e));
                break;
            case 'Explosion':
                if (e.sound) {
                    SoundManager.Instance.stopSound('explosion');
                    SoundManager.Instance.playSound('explosion', false);
                }
                let exp = new Explosion(e);
                Stage.Instance.addEntity(exp);
                if (exp.sprite === 'shootSmoke') {
                    for (let tk of Stage.Instance.entities['Tank']) {
                        if (tk.id === exp.idTank) {
                            let plusx = (Math.cos(tk.rotation) * (tk.h + 18)) / 2;
                            let plusy = (Math.sin(tk.rotation) * (tk.h + 18)) / 2;
                            exp.followEntity(tk, plusx, plusy);
                            break;
                        }
                    }
                }
                break;
            case 'Bullet':
                SoundManager.Instance.stopSound(e.soundShot);
                SoundManager.Instance.playSound(e.soundShot, false);
                Stage.Instance.addEntity(new Bullet(e));
                break;
            case 'PowerUp':
                Stage.Instance.addEntity(new PowerUp(e));
                break;
        }
    });

    NetworkManager.Instance.addEventListener('entities_map_updated', function(data) {
        let entities = data.entities;
        if (entities !== undefined) {
            for (let group in entities) {
                for (let i = 0; i < entities[group].length; ++i) {
                    let e = entities[group][i];
                    switch (e.constructorName) {
                        case 'Tank':
                            Stage.Instance.entities[group][i].getDataFromClone(e);
                            break;
                        case 'Explosion':
                            Stage.Instance.entities[group][i].getDataFromClone(e);
                            break;
                        case 'Bullet':
                            Stage.Instance.entities[group][i].getDataFromClone(e);
                            break;
                        case 'PowerUp':
                            Stage.Instance.entities[group][i].getDataFromClone(e);
                            break;
                    }
                }
            }
        }
        Stage.Instance.Map.renderBlockChanges(data.mapBlockChanges);
    });

    NetworkManager.Instance.addEventListener('main_player_join', function(playerIndex) {
        let entity = Stage.Instance.entities['Tank'][playerIndex];
        Stage.Instance.addMainPlayer(entity);
    });

    NetworkManager.Instance.addEventListener('pingResponsed', function(data) {
        Stage.Instance.mainPlayer.ping = data.timeToServer + (Date.now() - data.timeServer);
    });

    InputManager.Instance;
}

function draw() {
    Stage.Instance.update();
    Stage.Instance.render();
}

function windowResized() {
    /* resizeCanvas(windowWidth * 0.9, windowHeight * 0.9);*/

    /*Stage.Instance.cvn= createCanvas(windowWidth , windowHeight );*/
    width = windowWidth;
    height = windowHeight;
    /*let x = (windowWidth -width) / 2;
    let y =(windowHeight - height)/2;*/
    Stage.Instance.cnv.position(0, 0);
    Map.Instance().onResize();
    console.log('resize');
}
