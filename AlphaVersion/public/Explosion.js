class Explosion {
    //constructor(sprite, x, y, width, height, cantimages) {
    //  Object.assign(this, {
    //    get sprite() {
    //      return Assets.get(sprite);
    //    },
    //    get spritex() {
    //      return -this.sprite.width / cantimages;
    //    },
    //    get img() {
    //      return this.sprite.get(0, 0, this.sprite.width / cantimages, this.sprite.height);
    //    },
    //    get width() {
    //      return width > this.sprite.width / cantimages ? this.sprite.width / cantimages : width;
    //    },
    //    get height() {
    //      return height > this.sprite.height ? this.sprite.height : height;
    //    },
    //  });
    //  this.cantimages = cantimages;
    //  this.deleteEntity = false;
    //  this.imgActual = 1;
    //  this.rate = 0;
    //  this.x = x;
    //  this.y = y;
    //}

    constructor(explosionClone) {
        this.getDataFromClone(explosionClone);
    }

    getDataFromClone(explosionClone) {
        Object.assign(this, explosionClone);
    }

    render() {
        push();
        translate(this.x, this.y);
        let imgw;
        let imgh;
        let img;
        try {
            imgw = Assets.get(this.sprite).width / this.cantimages;
            imgh = Assets.get(this.sprite).height;
            img = Assets.get(this.sprite).get(imgw * (this.imgActual - 1), 0, imgw, imgh);
            this.width = this.width > imgw ? imgw : this.width;
            this.height = this.height > imgh ? imgh : this.height;
            image(img, -this.width / 2, -this.height / 2, this.width, this.height);
        } catch (err) {
            console.log(img);
            console.log(imgw);
            console.log(imgh);
            console.log(err);
            console.log(this);
        }
        pop();
    }

    //update() {
    //   this.rate += 1;
    //   if (this.rate > 2) {
    //      this.rate = 0;
    //      this.imgActual += 1;
    //      if (this.imgActual === this.cantimages + 1) {
    //         this.deleteEntity = true;
    //      } else {
    //         this.spritex += this.sprite.width / this.cantimages;
    //         this.img = this.sprite.get(this.spritex, 0, this.sprite.width / this.cantimages, this.sprite.height);
    //      }
    //   }
    //}
    update() {
        if (this.followedEntity !== undefined) {
            this.x = this.followedEntity.x + this.plusx;
            this.y = this.followedEntity.y + this.plusy;
        }
        this.rate += 1;
        if (this.rate > 5) {
            this.rate = 0;
            this.imgActual += 1;
            if (this.imgActual > this.cantimages) {
                this.imgActual=1;
                this.deleteEntity = true;
            }
        }
    }

    followEntity(e, plusx, plusy) {
        this.followedEntity = e;
        this.plusx = plusx;
        this.plusy = plusy;
    }
}
