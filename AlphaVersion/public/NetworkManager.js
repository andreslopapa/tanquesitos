class NetworkManager {
    constructor() {
        this.socket = io({transports: ['websocket'], upgrade: false});
    }

    static get Instance() {
        if (!this._intance) {
            this._intance = new NetworkManager();
        }
        return this._intance;
    }

    addEventListener(eventName, func) {
        if (func !== undefined) {
            this.socket.on(eventName, func);
        } else {
            this.socket.on(eventName);
        }
    }

    removeListener(eventName) {
        this.socket.off(eventName);
    }
}
