class Camera {
  constructor() {
    this.xOffset = 0;
    this.yOffset = 0;
  }

  /*	fix(){
		if(this.xOffset < 0)
			this.xOffset = 0;
		else if(this.xOffset > Map.Instance().numCols*Tile.SIZE*Map.Instance().scl - width){
			this.xOffset = Map.Instance().numCols*Tile.SIZE*Map.Instance().scl - width;
		}
		if(this.yOffset < 0)
			this.yOffset = 0;
		else if(this.yOffset > Map.Instance().numRows*Tile.SIZE*Map.Instance().scl - height){
			this.yOffset =  Map.Instance().numRows*Tile.SIZE*Map.Instance().scl - height;
		}
	}*/

  /*    move(amtX, amtY) {
        this.xOffset += amtX;
        this.yOffset += amtY;
        }*/

  followEntity(e) {
    if (
      Map.Instance().numRows * Tile.SIZE * Map.Instance().scl > height ||
      Map.Instance().numCols * Tile.SIZE * Map.Instance().scl > width
    ) {
      if (width > height) {
        this.xOffset = 0;
        this.yOffset = e.y * Map.Instance().scl - height / 2;
        if (this.yOffset < 0) this.yOffset = 0;
        else if (this.yOffset > Map.Instance().numRows * Tile.SIZE * Map.Instance().scl - height) {
          this.yOffset = Map.Instance().numRows * Tile.SIZE * Map.Instance().scl - height;
        }
      } else {
        this.xOffset = e.x * Map.Instance().scl - width / 2;
        this.yOffset = 0;
        if (this.xOffset < 0) this.xOffset = 0;
        else if (this.xOffset > Map.Instance().numCols * Tile.SIZE * Map.Instance().scl - width) {
          this.xOffset = Map.Instance().numCols * Tile.SIZE * Map.Instance().scl - width;
        }
      }
      /*this.xOffset = e.x - width/2;
		this.yOffset = e.y - height/2;*/
      /*this.fix();*/
    }
  }
}
