const Assets = require('./Assets.js');
const Bullet = require('./Bullet.js');
const Map = require('./Map.js');
const PowerUp = require('./PowerUp.js');
const Tank = require('./Tank.js');
const Tile = require('./Tile.js').Tile;
const Weapon = require('./Weapon.js');
const NetworkManager = require('./NetworkManager.js');
const Explosion = require('./Explosion.js');
const fs = require('fs');

class Stage {
    constructor() {
        this.entities = {Bullet: [], Explosion: [], PowerUp: [], Tank: []};
    }

    static get Instance() {
        if (this._instance === undefined) {
            this._instance = new Stage();
        }
        return this._instance;
    }

    addEntity(e) {
        e.constructorName = e.constructor.name;
        let index = this.entities[e.constructorName].length;
        this.entities[e.constructorName][index] = e;
        this.io.emit('entity_added', e);
    }

    removeEntity(e) {
        let index = this.entities[e.constructorName].indexOf(e);
        this.entities[e.constructorName].splice(index, 1);
    }

    addMainPlayer(p) {
        this.mainPlayer = p;
    }

    //	preloadAssets() {
    //Assets.initialize();
    //}

    //	loadJSONFile(path, callback) {
    //		var xobj = new XMLHttpRequest();
    //		xobj.overrideMimeType('application/json');
    //		xobj.open('GET', path, true);
    //		xobj.onreadystatechange = function() {
    //			if (xobj.readyState == 4 && xobj.status == '200') {
    //				// Required use of an anonymous callback as .open will NOT return a value but simply returns undefined in asynchronous mode
    //				callback(xobj.responseText);
    //			}
    //		};
    //		xobj.send(null);
    //	}

    initialize() {
        //this.cnv = createCanvas(displayWidth, displayHeight);
        //this.cnv.position(0, 0);
        //width = windowWidth;
        //height = windowHeight;

        Assets.initialize();
        Assets.registerAsset('map', JSON.parse(fs.readFileSync('./server/maps/mapa1.json', 'utf8')));
        this.currentMapJSON = Assets.get('map');
        this.Map = Map.Instance(Assets.get('map'), this.entities);
        //this.camera = new Camera(this.Map.numCols, this.Map.numRows);
        Stage.Instance.addEntity(new PowerUp(PowerUp.Type.WEAPON.HeavyMachineGun, 12 * Tile.SIZE, 12 * Tile.SIZE, Tile.SIZE, Tile.SIZE));

        //this.Map.onResize();

        //create powerups
        setInterval(function() {
            if (Stage.Instance.entities['PowerUp'].length < 5) {
                let xAppear = Math.floor(Math.random() * (Stage.Instance.Map.numCols - 1)) * Tile.SIZE;
                let yAppear = Math.floor(Math.random() * (Stage.Instance.Map.numRows - 1)) * Tile.SIZE;
                if (Stage.Instance.Map.getThereACollidable(xAppear, yAppear, Tile.SIZE, Tile.SIZE) === null) {
                    let powerupsNames = [];
                    for (let pu in PowerUp.Type) {
                        if (typeof PowerUp.Type[pu] === 'object') {
                            powerupsNames = powerupsNames.concat(Object.keys(PowerUp.Type[pu]));
                        } else {
                            powerupsNames[powerupsNames.length] = PowerUp.Type[pu];
                        }
                    }
                    //let index = powerupsNames.indexOf('Default');
                    //if (index > -1) {
                    //    powerupsNames.splice(index, 1);
                    //}
                    let powerupRandom = powerupsNames[Math.floor(powerupsNames.length * Math.random())];
                    if (Stage.Instance.Map.getAPowerUp(xAppear, yAppear, Tile.SIZE, Tile.SIZE) === null) {
                        Stage.Instance.addEntity(new PowerUp(powerupRandom, xAppear, yAppear, Tile.SIZE, Tile.SIZE));
                    }
                }
            }
        }, 2000);
    }

    //	handleKeysReleased(){
    //		this.mainPlayer.handleKeysReleased();
    //	}

    //	render() {
    //		background(0);
    //		push();
    //		this.Map.render(this.camera);
    //		for (let entity of this.entities) {
    //			if ((entity === this.mainPlayer)||(entity instanceof Bullet)) continue;
    //			entity.render();
    //		}
    //		//render mainPlayer on top of other players
    //		this.mainPlayer.render();
    //
    //		//render bullets on top
    //		for (let bullet of this.bullets) {
    //			bullet.render();
    //		}
    //		fill(255);
    //		noStroke();
    //		textSize(18);
    //		text(floor(frameRate()), 60, 60);
    //		pop();
    //
    //	}

    update(dt) {
        for (let group in this.entities) {
            for (let entity of this.entities[group]) {
                entity.update(dt);
            }
        }
        for (let i = this.entities['Bullet'].length - 1; i >= 0; --i) {
            let b = this.entities['Bullet'][i];
            let condition = b.x < 0 || b.y < 0 || b.x > this.Map.numCols * Tile.SIZE || b.y > this.Map.numRows * Tile.SIZE;
            if (condition) {
                this.entities['Bullet'].splice(i, 1);
                this.io.emit('entity_removed', {group: 'Bullet', index: i});
            }
        }
        for (let tank of this.entities['Tank']) {
            if (tank.destroyed) {
                tank.streaKills = 0;
                if (tank.reappear) {
                    tank.deaths++;
                    tank.reappear = false;
                    tank.destroyed = false;
                    let reappearPoints = [
                        {px: 3 * Tile.SIZE, py: 3 * Tile.SIZE},
                        {px: 21 * Tile.SIZE, py: 3 * Tile.SIZE},
                        {px: 3 * Tile.SIZE, py: 21 * Tile.SIZE},
                        {px: 21 * Tile.SIZE, py: 21 * Tile.SIZE},
                    ];
                    let poinDistances = [];
                    let pointToAppear = {};
                    let maxDistance = 0;
                    for (let point of reappearPoints) {
                        let distance = 0;
                        for (let t of this.entities['Tank']) {
                            if (t.destroyed) {
                                continue;
                            }
                            distance += Math.sqrt(Math.pow(point.px - t.x, 2) + Math.pow(point.py - t.y, 2));
                        }
                        if (distance > maxDistance) {
                            maxDistance = distance;
                            pointToAppear = point;
                        }
                    }
                    let pxAppear = pointToAppear.px + tank.w / 2;
                    let pyAppear = pointToAppear.py + tank.h / 2;
                    if (
                        this.Map.getThereACollidable(pxAppear, pyAppear, tank.w, tank.h) === null &&
                        this.Map.getATank(pxAppear, pyAppear, tank.w, tank.h, tank.id) === null
                    ) {
                        tank.x = pxAppear;
                        tank.y = pyAppear;
                    }
                }
            }
            for (let i = 0; i < tank.bullets.length; ++i) {
                this.addEntity(tank.bullets[i]);
                let plusx = (Math.cos(tank.rotation) * (tank.h + 18)) / 2;
                let plusy = (Math.sin(tank.rotation) * (tank.h + 18)) / 2;
                let xe = tank.x + plusx;
                let ye = tank.y + plusy;
                let explosion = new Explosion('shootSmoke', xe, ye, Tile.SIZE / 1.5, Tile.SIZE / 1.5, 6, false, tank.id);
                explosion.followEntity(tank, plusx, plusy);
                this.addEntity(explosion);
            }
            tank.bullets = [];
        }
        for (let bullet of this.entities['Bullet']) {
            if (bullet.tanksDestroyed > 0) {
                for (let tank of this.entities['Tank']) {
                    if (tank.id === bullet.idTank) {
                        tank.streaKills += bullet.tanksDestroyed;
                        tank.kills += bullet.tanksDestroyed;
                        break;
                    }
                }
            }
            for (let i = 0; i < bullet.explosions.length; ++i) {
                this.addEntity(bullet.explosions[i]);
            }
            bullet.explosions = [];
        }
        for (let group in this.entities) {
            for (let i = this.entities[group].length - 1; i >= 0; --i) {
                if (this.entities[group][i].deleteEntity === true) {
                    this.entities[group].splice(i, 1);
                    this.io.emit('entity_removed', {group: group, index: i});
                }
            }
        }
    }
}
module.exports = Stage;
