const Stage = require('./Stage.js');
const Bullet = require('./Bullet.js');

class Weapon {
    constructor(type) {
        this.type = type;
        switch (type) {
            case Weapon.Type.HeavyMachineGun:
                this.letter="H";
                this.munition=80;
                this.originalMunition=80;
                this.reloadCounter=0;
                this.reloadRate=0;
                this.reloadRateLimit=3;
                this.fireRateCounter = 6;
                this.limitFireRate = 5;
                this.damage = 50;
                this.speedBullet = 10;
                this.wBullet = 4;
                this.hBullet = 1;
                this.soundShot = 'shot';
                this.spriteBullet = 'shootMachineGun';
                this.imageIsFunction = true;
                //this.spriteBullet =function() {
                //  stroke(255, 0, 0);
                //  strokeWeight(2);
                //  line(0, 0, 4, 0);
                //};
                break;
            case Weapon.Type.RocketLauncher:
                this.letter="R";
                this.munition=20;
                this.reloadCounter=0;
                this.reloadRate=0;
                this.reloadRateLimit=14;
                this.originalMunition=20;
                this.fireRateCounter = 20;
                this.limitFireRate = 19;
                this.damage = 400;
                this.speedBullet = 3;
                this.wBullet = 37;
                this.hBullet = 20;
                this.soundShot = 'missilelaunched';
                this.spriteBullet = 'misiles';
                this.imageIsFunction = false;
                this.cantimages = 7;
                break;
            case Weapon.Type.SuperGrenade:
                this.letter="S";
                this.munition=40;
                this.reloadCounter=0;
                this.reloadRate=0;
                this.reloadRateLimit=7;
                this.originalMunition=40;
                this.fireRateCounter = 14;
                this.limitFireRate = 13;
                this.damage = 100;
                this.speedBullet = 5;
                this.wBullet = 13;
                this.hBullet = 8;
                this.soundShot = 'shot';
                this.spriteBullet = 'shootSuperGrenade';
                this.imageIsFunction = true;
                this.cantimages = 1;
                break;
        }
    }
}
Weapon.Type = {
    SuperGrenade: 'SuperGrenade',
    HeavyMachineGun: 'HeavyMachineGun',
    RocketLauncher: 'RocketLauncher',
};
module.exports = Weapon;
