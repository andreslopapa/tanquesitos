const Assets = require('./Assets.js');
const Map = require('./Map.js');
const Tile = require('./Tile.js').Tile;
const Bullet = require('./Bullet.js');

class Tank {
    constructor(sprite, x, y, cantimages, weapon, colorTank) {
        this.sprite = sprite;
        this.spriteDestruction = 'mushroomexplosion';
        this.cantimagesDestruction = 10;
        this.imgActual = 1;
        this.cantimages = cantimages;
        this.deleteEntity = false;
        this.rate = 0;
        this.w = Tile.SIZE * 0.8;
        this.h = Tile.SIZE * 0.8;
        this.y = y + this.w / 2;
        this.x = x + this.h / 2;
        this.linearSpeed = 2;
        this.rotationSpeed = 0.05;
        this.rotation = 0;
        this.rSpeed = 0;
        this.xMove = 0;
        this.yMove = 0;
        this.weapon = weapon;
        this.moving = false;
        this.bullets = [];
        this.life = 500;
        this.originalLife = 500;
        this.id = Tank.GetNewId();
        this.kills = 0;
        this.streaKills = 0;
        this.deaths = 0;
        this.destroyed = false;
        this.colorTank = colorTank;
        this.ping = 50;
        this.reappear = false;
        this.shield = 0;
        this.originalShield = 1;
    }
    static GetNewId() {
        if (Tank.LastId === undefined) {
            Tank.LastId = 0;
        }
        ++Tank.LastId;
        return Tank.LastId;
    }

    destroy(damage) {
        this.shield -= damage;
        if (this.shield < 0) {
            this.life += this.shield;
            this.shield = 0;
            if (this.life < 0) {
                this.destroyed = true;
                this.rate = 0;
                this.imgActual = 1;
            }
        }
    }

    handleKeysPressed() {
        let mapKeys = this.mapKeys;
        this.rSpeed = 0;
        this.xMove = 0;
        this.yMove = 0;
        if (mapKeys.DOWN || mapKeys.UP) {
            this.rate += 1;
            if (this.rate > 4) {
                this.rate = 0;
                this.imgActual += mapKeys.DOWN ? 1 : -1;
                if (this.imgActual > this.cantimages) {
                    this.imgActual = 1;
                } else if (this.imgActual < 1) {
                    this.imgActual = this.cantimages;
                }
            }
            if (mapKeys.UP && (mapKeys.LEFT || mapKeys.RIGHT)) {
                let dr = 0;
                if (mapKeys.LEFT) {
                    dr -= this.rotationSpeed;
                }
                if (mapKeys.RIGHT) {
                    dr += this.rotationSpeed;
                }
                this.rSpeed = dr;
            } else if (mapKeys.DOWN && (mapKeys.LEFT || mapKeys.RIGHT)) {
                let dr = 0;
                if (mapKeys.LEFT) {
                    dr += this.rotationSpeed;
                }
                if (mapKeys.RIGHT) {
                    dr -= this.rotationSpeed;
                }
                this.rSpeed = dr;
            }
            this.xMove += (mapKeys.UP === true ? 1 : -1) * this.linearSpeed * Math.cos(this.rotation);
            this.yMove += (mapKeys.UP === true ? 1 : -1) * this.linearSpeed * Math.sin(this.rotation);
            this.moving = true;
        } else if (mapKeys.LEFT || mapKeys.RIGHT) {
            let dr = 0;
            if (mapKeys.LEFT) {
                dr -= this.rotationSpeed;
            }
            if (mapKeys.RIGHT) {
                dr += this.rotationSpeed;
            }
            this.rSpeed = dr;
            this.moving = true;
        }
        //90 es la z
        if (mapKeys.SHOOT) {
            if (this.weapon.fireRateCounter > this.weapon.limitFireRate && this.weapon.munition > 0) {
                this.weapon.fireRateCounter = 0;
                this.weapon.munition--;
                let xB = this.x + (Math.cos(this.rotation) * this.h) / 2;
                let yB = this.y + (Math.sin(this.rotation) * this.h) / 2;
                let w = this.weapon;
                this.bullets.push(
                    new Bullet(
                        w.spriteBullet,
                        xB,
                        yB,
                        w.wBullet,
                        w.hBullet,
                        w.speedBullet,
                        this.rotation,
                        w.damage,
                        w.cantimages,
                        w.soundShot,
                        w.imageIsFunction,
                        this.id,
                        this.colorTank,
                    ),
                );
            }
        }
    }

    handleKeysReleased(mapKeys) {
        if (!(mapKeys.UP || mapKeys.DOWN || mapKeys.LEFT || mapKeys.RIGHT)) {
            this.moving = false;
        }
        //if (this.mapKeys !== undefined && this.mapKeys.SHOOT && !mapKeys.SHOOT) {
        //    this.weapon.fireRateCounter = this.weapon.limitFireRate + 1;
        //}
        this.mapKeys = mapKeys;
    }

    update() {
        if (!this.destroyed) {
            if (this.mapKeys !== undefined) {
                this.handleKeysPressed();
            }
            if (this.weapon.fireRateCounter <= this.weapon.limitFireRate) {
                ++this.weapon.fireRateCounter;
            }
            if (this.weapon.munition <= 0) {
                if (this.weapon.reloadRate > this.weapon.reloadRateLimit) {
                    this.weapon.reloadRate = 0;
                    this.weapon.reloadCounter++;
                    if (this.weapon.reloadCounter === this.weapon.originalMunition) {
                        this.weapon.munition = this.weapon.originalMunition;
                        this.weapon.reloadCounter = 0;
                    }
                } else {
                    this.weapon.reloadRate++;
                }
            }
            this.rotation += this.rSpeed;
            let xEvaluated = this.x + this.xMove;
            let yEvaluated = this.y + this.yMove;
            let colli = Map.Instance().getThereACollidable(xEvaluated, yEvaluated, this.w + 5, this.h + 5);
            let otherTank = Map.Instance().getATank(xEvaluated, yEvaluated, this.w + 5, this.h + 5, this.id);
            if (colli === null && otherTank === null) {
                this.x += this.xMove;
                this.y += this.yMove;
            } else {
                /*this.rSpeed += this.rotationSpeed*(-1)+0.3*Math.sign(this.rotationSpeed);*/
                /*puntos medios  cuadrado tanque*/
                xEvaluated = this.x + this.xMove;
                yEvaluated = this.y;
                otherTank = Map.Instance().getATank(xEvaluated, yEvaluated, this.w + 5, this.h + 5, this.id);
                colli = Map.Instance().getThereACollidable(xEvaluated, yEvaluated, this.w + 5, this.h + 5);
                if (colli === null && otherTank === null) {
                    this.x += this.xMove;
                } else {
                    xEvaluated = this.x;
                    yEvaluated = this.y + this.yMove;
                    otherTank = Map.Instance().getATank(xEvaluated, yEvaluated, this.w + 5, this.h + 5, this.id);
                    colli = Map.Instance().getThereACollidable(xEvaluated, yEvaluated, this.w + 5, this.h + 5);
                    if (colli === null && otherTank === null) {
                        this.y += this.yMove;
                    }
                }
            }
            let pu = Map.Instance().getAPowerUp(xEvaluated, yEvaluated, this.w, this.h);
            if (pu !== null) {
                pu.getPower.call(this);
                pu.deleteEntity = true;
            }
        } else {
            if (!this.reappear) {
                this.rate += 1;
                if (this.rate > 6) {
                    this.rate = 0;
                    this.imgActual += 1;
                    if (this.imgActual > this.cantimagesDestruction) {
                        this.imgActual = 1;
                        this.life = this.originalLife;
                        this.shield=5000;
                        setTimeout(()=>{this.shield=0},5000);
                        this.weapon.munition = this.weapon.originalMunition;
                        this.reappear = true;
                    }
                }
            }
        }
    }
}
module.exports = Tank;
