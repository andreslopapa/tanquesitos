const Assets = require('./Assets.js');
const PowerUp = require('./PowerUp.js');
const Tile = require('./Tile.js').Tile;

class Map {
    static Instance(map, entities) {
        if (this._instance === undefined) {
            this._instance = new Map(map, entities);
        }
        return this._instance;
    }

    constructor(map, entities) {
        this.entities = entities;
        this.hasBeenResized = true;
        this.scl = 1;
        this.layers = map.layers;
        this.numRows = map.height;
        this.numCols = map.width;
        this.blockChanges = [];
        // this.loadSpritesTiles(map);

        //this.backgroundImage = createImage(
        //  Math.floor(Map.numCols * Tile.SIZE),
        //  Math.floor(Map.numRows * Tile.SIZE),
        //);
        this.map3DTiles = [];
        for (let i = 0; i < this.numRows; ++i) {
            this.map3DTiles[i] = [];
            for (let j = 0; j < this.numCols; ++j) {
                this.map3DTiles[i][j] = [];
            }
        }
        this.collidables = map.layers[0].objects;
        for (let l = 0; l < this.layers.length; ++l) {
            if (this.layers[l].type === 'objectgroup') {
                continue;
            }
            for (let r = 0; r < this.numRows; ++r) {
                for (let c = 0; c < this.numCols; ++c) {
                    let index = c + r * this.numCols;
                    let idImg = this.layers[l].data[index];
                    let x = c * Tile.SIZE;
                    let y = r * Tile.SIZE;
                    let breakable = true;
                    let resistance = 0;
                    if (this.layers[l].name === 'bloques') {
                        let collidable = this.collidables.filter(c => c.x === x && c.y === y);
                        if (collidable[0] !== undefined) {
                            breakable = !collidable[0].properties.unbreakable;
                            resistance = collidable[0].properties.resistance;
                        }
                    }

                    this.map3DTiles[r][c][l] = new Tile(idImg, x, y, breakable, resistance);
                }
            }
        }
    }

    getThereACollidable(x, y, w, h) {
        let collidableC = posX => {
            return Math.trunc(posX / Tile.SIZE);
        };
        let collidableR = posY => {
            return Math.trunc(posY / Tile.SIZE);
        };

        // let index = c + r * (this.numCols);
        let c;
        let points = [
            {px: x - w / 2, py: y},
            {px: x + w / 2, py: y},
            {px: x, py: y - h / 2},
            {px: x, py: y + h / 2},
            {px: x - w / 2, py: y - h / 2},
            {px: x + w / 2, py: y - h / 2},
            {px: x - w / 2, py: y + h / 2},
            {px: x + w / 2, py: y + h / 2},
        ];
        for (let l = 0; l < this.map3DTiles[0][0].length; ++l) {
            for (let i = 0; i < points.length; ++i) {
                c = this.map3DTiles[collidableR(points[i].py)][collidableC(points[i].px)][l];
                if (c !== undefined && !c.walkable()) {
                    return c;
                }
            }
        }
        return null;
    }

    getAPowerUp(x, y, w, h) {
        let getColumna = posX => {
            return Math.trunc(posX / Tile.SIZE);
        };
        let getFila = posY => {
            return Math.trunc(posY / Tile.SIZE);
        };
        let points = [
            {px: x, py: y - h / 2},
            {px: x, py: y + h / 2},
            {px: x - w / 2, py: y - h / 2},
            {px: x + w / 2, py: y - h / 2},
            {px: x - w / 2, py: y + h / 2},
            {px: x + w / 2, py: y + h / 2},
            {px: x - w / 2, py: y},
            {px: x + w / 2, py: y},
        ];
        for (let p of this.entities['PowerUp']) {
            for (let i = 0; i < points.length; ++i) {
                if (getColumna(p.x) === getColumna(points[i].px) && getFila(p.y) === getFila(points[i].py)) {
                    return p;
                }
            }
        }
        return null;
    }

    getATank(x, y, w, h,idTankToAvoid) {
        let points = [
            {px: x, py: y - h / 2},
            {px: x, py: y + h / 2},
            {px: x - w / 2, py: y - h / 2},
            {px: x + w / 2, py: y - h / 2},
            {px: x - w / 2, py: y + h / 2},
            {px: x + w / 2, py: y + h / 2},
            {px: x - w / 2, py: y},
            {px: x + w / 2, py: y},
        ];
        for (let t of this.entities['Tank']) {
            if((idTankToAvoid!==undefined && t.id===idTankToAvoid) || t.destroyed){
                continue;
            }
            for (let i = 0; i < points.length; ++i) {
                let tll = t.x - t.w / 2;
                let tlr = t.x + t.w / 2;
                let tlb = t.y + t.h / 2;
                let tlt = t.y - t.h / 2;
                let condition = points[i].px > tll && points[i].px < tlr && points[i].py < tlb && points[i].py > tlt;
                if (condition) {
                    return t;
                }
            }
        }
        return null;
    }



}
module.exports = Map;
