const Weapon = require('./Weapon.js');
class PowerUp {
    constructor(type, x, y, w, h) {
        if (PowerUp.Type.WEAPON.hasOwnProperty(type)) {
            this.getPower = function() {
                this.weapon = new Weapon(type);
            };
        } else {
            switch (type) {
                case PowerUp.Type.LIFE:
                    this.getPower=function(){
                        this.life=this.originalLife;
                    }
                    break;
                case PowerUp.Type.SHIELD:
                    this.getPower=function(){
                        this.shield=500;
                        this.originalShield=500;
                    }
            }
        }
        this.img = type + 'img';
        this.x = x;
        this.y = y;
        this.w = w;
        this.h = h;
        this.deleteEntity = false;
        this.type = type;
    }

    //render() {
    //   push();
    //   translate(this.x, this.y);
    //   image(Assets.get(this.img), 0, 0, this.w, this.h);
    //   pop();
    //}
    update() {}
}
PowerUp.Type = {
    WEAPON: Weapon.Type,
    LIFE: 'Life',
    SHIELD:'Shield',
};
module.exports = PowerUp;
