class NetworkManager {

  constructor() {
    this.socket = io();
  }

  static get Instance() {
    if (!this._intance) {
      this._intance = new NetworkManager();
    }
    return this._intance;
  }

  addEventListener(eventName, func) {
    if (func !== undefined) {this.socket.on(eventName, func);}
    else {this.socket.on(eventName);}
    console.log(this.socket);	  
  }

  removeListener(eventName) {
    this.socket.off(eventName);
  }

}
module.exports=NetworkManager;
