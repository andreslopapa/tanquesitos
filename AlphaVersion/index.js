const express = require('express');
const path = require('path');
const app = express();
const server = require('http').createServer(app);
const mongoose = require('mongoose');
const bodyParser = require('body-parser');
const cors = require('cors');

const io = require('socket.io')(server);
const Stage = require('./server/Stage.js');
const Tank = require('./server/Tank.js');
const Weapon = require('./server/Weapon.js');
const Tile = require('./server/Tile.js').Tile;
const Assets = require('./server/Assets.js');

app.use(cors());
app.use(bodyParser.urlencoded({extended: false}));
/*Returns middleware that only parses urlencoded bodies and only looks
 * at requests where the Content-Type header matches the type option. This parser
 * accepts only UTF-8 encoding of the body and supports automatic inflation of gzip and deflate encodings.*/
/*The extended option allows to choose between parsing the URL-encoded data with the querystring
 * library (when false) or the qs library (when true). The "extended" syntax allows for
 * rich objects and arrays to be encoded into the URL-encoded format, allowing for a JSON-like experience with URL-encoded*/

app.use(bodyParser.json());
mongoose.connect('mongodb://localhost/tanques');
require('./models/player.js');
app.use(require('./routes'));
var router = express.Router();
app.use(router);

const port = 3000;
app.use(express.static(path.join(__dirname, 'public')));
server.listen(port, '0.0.0.0', function() {
    console.log('Tanquesitos corriendo en puerto ' + port);
});
app.use(function(req, res) {
    res.sendStatus(404);
});

Stage.Instance.io = io;
io.set('transports', ['websocket']); 
Stage.Instance.initialize();

let p;
io.on('connection', function(socket) {
    let SI = Stage.Instance;
    socket.emit('current_map_entities', {map: SI.Map, mapJSON: SI.currentMapJSON});
    if (SI.entities['Tank'].length === 0) {
        p = new Tank('bluetank', 3 * Tile.SIZE, 3 * Tile.SIZE, 7, new Weapon(Weapon.Type.SuperGrenade), 'blue');
    } else if (SI.entities['Tank'].length === 1) {
        p = new Tank('redtank', 21 * Tile.SIZE, 3 * Tile.SIZE, 7, new Weapon(Weapon.Type.SuperGrenade), 'red');
    } else if (SI.entities['Tank'].length === 2) {
        p = new Tank('greentank', 3 * Tile.SIZE, 21 * Tile.SIZE, 7, new Weapon(Weapon.Type.SuperGrenade), '#006600');
    } else if (SI.entities['Tank'].length === 3) {
        p = new Tank('pinktank', 21 * Tile.SIZE, 21 * Tile.SIZE, 7, new Weapon(Weapon.Type.SuperGrenade), '#cc3399');
    }
    p.constructorName = p.constructor.name;
    socket.player = p;
    Stage.Instance.addEntity(p);
    socket.emit('main_player_join', SI.entities['Tank'].indexOf(p));
    console.log('hay ' + SI.entities['Tank'].length + ' jugadores');
    socket.on('key_press', function(mapKeys) {
        socket.player.mapKeys = mapKeys;
    });
    socket.on('key_release', function(mapKeys) {
        socket.player.handleKeysReleased(mapKeys);
    });
    socket.on('pingRequested', function(data) {
        socket.player.ping = data.pingMainPlayer;
        let timeToServer = Date.now() - data.timeClient;
        socket.emit('pingResponsed', {timeToServer: timeToServer, timeServer: Date.now()});
    });
    socket.on('disconnect', function() {
        io.emit('player_leave', SI.entities['Tank'].indexOf(p));
        Stage.Instance.removeEntity(p);
    });
});
let updateInterval = setInterval(function() {
    Stage.Instance.update();
}, 17);
let updateClientInterval = setInterval(function() {
    let entitiesToSend = Object.assign({}, Stage.Instance.entities);
    delete entitiesToSend['PowerUp'];
    delete entitiesToSend['Explosion'];
    io.emit('entities_map_updated', {entities: entitiesToSend, mapBlockChanges: Stage.Instance.Map.blockChanges});
    Stage.Instance.Map.blockChanges = [];
}, 100);

//socket.emit('message', "this is a test");  sending to sender-client only
//socket.broadcast.emit('message', "this is a test");  sending to all clients except sender
//socket.broadcast.to('game').emit('message', 'nice game');  sending to all clients in 'game' room(channel) except sender
//socket.to('game').emit('message', 'enjoy the game');  sending to sender client, only if they are in 'game' room(channel)
//socket.broadcast.to(socketid).emit('message', 'for your eyes only');  sending to individual socketid
//io.emit('message', "this is a test");  sending to all clients, include sender
//io.in('game').emit('message', 'cool game');  sending to all clients in 'game' room(channel), include sender
//io.of('myNamespace').emit('message', 'gg');  sending to all clients in namespace 'myNamespace', include sender
//socket.emit();  send to all connected clients
//socket.broadcast.emit();  send to all connected clients except the one that sent the message
//socket.on();  event listener, can be called on client to execute on server
//io.sockets.socket();  for emiting to specific clients
//io.sockets.emit();  send to all connected clients (same as socket.emit)
//io.sockets.on() ;  initial connection from a client.
