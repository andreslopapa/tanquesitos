const express = require('express');
const path = require('path');
const app = express();
const server = require('http').createServer(app);
const bodyParser=require('body-parser');
const cors=require('cors');
app.use(cors());

const port=3000;
app.use(express.static(path.join(__dirname, 'public')));
server.listen(port, '0.0.0.0', function() {
	  console.log('Mapita animado corriendo en puerto 3000');

});
app.use(function(req,res){res.sendStatus(404);});

