function preload() {
    Stage.Instance.preloadAssets();
}

function setup() {
	
    Stage.Instance.initialize();

}


function draw() {
    Stage.Instance.update(1/60);
    Stage.Instance.render();
}