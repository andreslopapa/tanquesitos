class Map{
	
	static load(tmxMap){
		this.width = tmxMap.width;
		this.height = tmxMap.height;
		this.layers = {};
		this.collidables;
		this.animatedTiles = [];
		this.hasChanges = true;
		this.backgroundImage = createImage(this.width * Tile.Size, this.height * Tile.Size);
		this.scl = 1;
		
		for(let layer of tmxMap.layers){
			if(layer.type === "group"){
			this.layers[layer.name]  = layer.layers;
			}else if(layer.type === "tilelayer"){
			 this.layers[layer.name] = layer;
			}else if(layer.type === "objectgroup"){
			  this.collidables = layer.objects;
			}
		}
		
		this._animatedTiles = {};
		
		for(let at of tmxMap.tilesets[0].tiles){
			this._animatedTiles[at.id] = at.animation;
		}
	
		
		this._map = [];
		let layerIndex = -1;
		for(let name of Object.keys(this.layers)){
		let layer = this.layers[name];
		if(layer.length){
			for(let _layer of layer){
				layerIndex++;
				this._map[layerIndex] = {};
			for(var x = 0;x<this.width;x++){
				this._map[layerIndex][x] = {}; 
				for(var y=0;y<this.height;y++){
					let index = x + y * this.width;
					let tileID = _layer.data[index] - 1;
					if(this._animatedTiles[tileID])
						this._map[layerIndex][x][y] = new AnimatedTile(this._animatedTiles[tileID],x,y);
					else
						this._map[layerIndex][x][y] = tileID; 
				}
				}
			}
		}else{
			layerIndex++;
			this._map[layerIndex] = {};
			for(var x = 0;x<this.width;x++){
				this._map[layerIndex][x] = {}; 
				for(var y=0;y<this.height;y++){
					let index = x + y * this.width;
					let tileID = layer.data[index] - 1;
					if(tileID === -1)
					this._map[layerIndex][x][y] = tileID;
					else
					this._map[layerIndex][x][y] = new StaticTile(tileID,x,y); 
				}
				}
		}		
	

		}
		
		this.loadTileImages();
	}
	
	static loadTileImages(){
		let sprite = Assets.get("mapsprite");
		for(let name of Object.keys(this.layers)){
			let layer = this.layers[name];
			if(layer.length){
				for(let _layer of layer){
					for(let tileID of _layer.data){
						let realID = tileID - 1;
						if(realID === -1) continue;
						let x = Math.floor(realID % Math.floor(sprite.width / Tile.Size));
						let y = Math.floor(realID / Math.floor(sprite.width / Tile.Size));
						TileManager.registerTileImage(realID,sprite.get(x * Tile.Size, y * Tile.Size,Tile.Size,Tile.Size));
					}	
				}
			}else{		
				for(let tileID of layer.data){
						let realID = tileID - 1;
						if(realID === -1) continue;
						let x = Math.floor(realID % Math.floor(sprite.width / Tile.Size));
						let y = Math.floor(realID / Math.floor(sprite.width / Tile.Size));
						TileManager.registerTileImage(realID,sprite.get(x * Tile.Size, y * Tile.Size,Tile.Size,Tile.Size));
				}
			}
		}
		for (let key of Object.keys(this._animatedTiles)){
			let tiles = this._animatedTiles[key]
			for (let tile of tiles){
				let tileID = tile.tileid;
				let x = Math.floor(tileID % Math.floor(sprite.width / Tile.Size));
				let y = Math.floor(tileID / Math.floor(sprite.width / Tile.Size));
				TileManager.registerTileImage(tileID,sprite.get(x * Tile.Size, y * Tile.Size,Tile.Size,Tile.Size));		
			}				
		}
		
	}
	
	static onResize(newWidth,newHeight){
		this.scl = 1/Math.max(this.width * Tile.Size / newWidth,this.height * Tile.Size / newHeight);
		this.hasChanges = true;
	}
	
	static drawTileImage(img,x,y){
		this.backgroundImage.blend(
             img,
              0,
              0,
              Tile.Size,
              Tile.Size,
              x * Tile.Size,
              y * Tile.Size,
              Tile.Size,
              Tile.Size,
              BLEND,
            );
	}
	
	static addTileAnimation(tileAnimation){
		this.animatedTiles.push(tileAnimation);
	}
	
	static update(dt){
		for(var i=0;i<this.animatedTiles.length;i++)
			this.animatedTiles[i].update(dt);
	}
	
	static requestRender(tile){
		let tx = tile.x;
		let ty = tile.y;
		for(let l = 0;l<this._map.length;l++){
		let _tile = this._map[l][tx][ty];
		if(_tile === -1) continue;
		if(_tile instanceof Tile){
		_tile.render()
		}else{
		Map.drawTileImage(TileManager.getTileImage(_tile),tx,ty);
		}
		}
	}
	
	static render(){
		if(this.hasChanges){
			this.hasChanges = false;
			for(var l = 0;l<this._map.length;l++){
			for(var y = 0;y<this.height;y++){
			for(var x = 0;x<this.width;x++){
				let tile = this._map[l][x][y];
				if(tile === -1) continue;
					if(tile instanceof Tile){
					tile.render();
				}else{
					Map.drawTileImage(TileManager.getTileImage(tile),x,y);
				}
			}
			}
			}
		}
		scale(this.scl);
		image(this.backgroundImage,0,0);
	}
	
}