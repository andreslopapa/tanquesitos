class Stage{
	
	  static get Instance() {
        if (this._instance === undefined) {
            this._instance = new Stage();
        }
        return this._instance;
    }
	
	preloadAssets(){
			Assets.initialize();
			Assets.registerAsset("map",loadJSON("map1.json"));
			Assets.registerAsset("mapsprite",loadImage("sprite.png"));
			Assets.registerAsset("tank",loadImage("redtank.png"));
			Assets.registerAsset("ambienceBeachWaves",loadSound("beachSound.mp3"));
	}
	
	initialize(){
		createCanvas(480,480);
		background(0);
		this.entities = new Set();
		Map.load(Assets.get("map"));
		Map.onResize(480,480);
		let sp = Assets.get("tank");
		this.mainPlayer = new Tank(sp.get(0,3 * 32,32,32),0,0);
		this.inputManager = new InputManager(this.mainPlayer.onMoveRequest.bind(this.mainPlayer));
		this.ambientSound = new AmbientSound("ambienceBeachWaves",5,5,5);
	//	this.sound = Assets.get("ambienceBeachWaves").loop();
	}
	
	addEntity(e) {
        this.entities.add(e);
    }
	
	render(){
		background(0);
		Map.render();
		for (let entity of this.entities) {
            entity.render();
        }
		this.mainPlayer.render();
		this.ambientSound.display();
	}
	
	update(dt){
		Map.update(dt);
		this.mainPlayer.update(dt);
		 for (let entity of this.entities) {
            entity.update(dt);
        }
		this.ambientSound.followEntity(this.mainPlayer);
	}
}