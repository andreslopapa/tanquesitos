class Keys{
	static get LEFT() {return 65;}
	static get RIGHT() {return 68;}
	static get UP() {return 87;}
	static get DOWN() {return 83;}
	static get BOMBKEY(){return 32;}
	
}

class InputManager {

	
	constructor(onKeyCallback) {
		this.moving = {RIGHT:false,LEFT:false,UP:false,DOWN:false};
		//Copy 
		this.lastMove = Object.assign({}, this.moving);
		let keyHandler =  (e) => {
			e = e || window.event;
			if (e.keyCode == Keys.LEFT) {
				this.moving.LEFT = (e.type == "keydown");
			} else if (e.keyCode == Keys.RIGHT) {
				this.moving.RIGHT = (e.type == "keydown");
			}else if (e.keyCode == Keys.UP) {
				this.moving.UP = (e.type == "keydown");
			}else if (e.keyCode == Keys.DOWN) {
				this.moving.DOWN = (e.type == "keydown");
			}
			
			if(JSON.stringify(this.moving) !== JSON.stringify(this.lastMove)){ 
			this.lastMove = Object.assign({}, this.moving);
				onKeyCallback(this.lastMove);
			}
			
		}
		document.body.onkeydown = keyHandler;
		document.body.onkeyup = keyHandler;
		
	}
}

class Tank extends Entity {
  constructor(img, x, y) {
	super(x +  0.4,y +  0.4,Tile.Size * 0.8,Tile.Size * 0.8);
    this.img = img;
    this.linearSpeed = Tile.Size/25;
    this.rotationSpeed = 2;
    this.rotation = 0;
    this.rSpeed = 0;
    this.xMove = 0;
    this.yMove = 0;
  }
  
  onMoveRequest(keyMap){

	  this.rSpeed = 0;
	  this.xMove = 0;
	  this.yMove = 0;
	  let dr = 0;
	  if (keyMap.DOWN || keyMap.UP) {
			if(keyMap.UP && keyMap.RIGHT)
				dr += this.rotationSpeed;
			if(keyMap.UP && keyMap.LEFT)
				dr -= this.rotationSpeed;
			if(keyMap.DOWN && keyMap.RIGHT)
				dr -= this.rotationSpeed;
			if(keyMap.DOWN && keyMap.LEFT)
				dr += this.rotationSpeed;
			
			if(!keyMap.RIGHT && !keyMap.LEFT){
			 this.xMove += (keyMap.UP   === true ? 1 : -1) * this.linearSpeed * cos(this.rotation);
			 this.yMove += (keyMap.UP === true ? 1 : -1) * this.linearSpeed * sin(this.rotation);
			}
	  }else if(keyMap.LEFT || keyMap.RIGHT){
		  if(keyMap.RIGHT)
				dr += this.rotationSpeed;
		  if(keyMap.LEFT)
				dr -= this.rotationSpeed;
	  }
	     this.rSpeed = dr;
  }
/*
  handleKeys() {
    this.rSpeed = 0;
    this.xMove = 0;
    this.yMove = 0;
    if (keyIsDown(DOWN_ARROW) || keyIsDown(UP_ARROW)) {
      if (
        keyIsDown(UP_ARROW) &&
        (keyIsDown(LEFT_ARROW) || keyIsDown(RIGHT_ARROW))
      ) {
        let dr = 0;
        if (keyIsDown(LEFT_ARROW)) {
          dr -= this.rotationSpeed;
        }
        if (keyIsDown(RIGHT_ARROW)) {
          dr += this.rotationSpeed;
        }
        this.rSpeed = dr;
      } else if (
        keyIsDown(DOWN_ARROW) &&
        (keyIsDown(LEFT_ARROW) || keyIsDown(RIGHT_ARROW))
      ) {
        let dr = 0;
        if (keyIsDown(LEFT_ARROW)) {
          dr += this.rotationSpeed;
        }
        if (keyIsDown(RIGHT_ARROW)) {
          dr -= this.rotationSpeed;
        }
        this.rSpeed = dr;
      }
      this.xMove +=
        (keyIsDown(UP_ARROW) === true ? 1 : -1) *
        this.linearSpeed *
        cos(this.rotation);
      this.yMove +=
        (keyIsDown(UP_ARROW) === true ? 1 : -1) *
        this.linearSpeed *
        sin(this.rotation);
      SoundManager.Instance.stopSound('engine');
      SoundManager.Instance.playSound('movetank', true);
    }else if(keyIsDown(LEFT_ARROW) || keyIsDown(RIGHT_ARROW)){
        let dr = 0;
        if (keyIsDown(LEFT_ARROW)) {
          dr -= this.rotationSpeed;
        }
        if (keyIsDown(RIGHT_ARROW)) {
          dr += this.rotationSpeed;
        }
        this.rSpeed = dr;
      SoundManager.Instance.stopSound('engine');
      SoundManager.Instance.playSound('movetank', true);
    }
   
  }
*/

  update(dt) {
    this.rotation += this.rSpeed * dt;
	this.x += this.xMove * dt;
	this.y += this.yMove * dt;
	
  }

  render() {
	
    push();
    translate(this.x * Tile.Size, this.y * Tile.Size);
    noFill();
    stroke(0, 255, 0);
    strokeWeight(2);
    rotate(this.rotation);
    image(this.img, -this.w / 2, -this.h / 2, this.w, this.h);
    pop();
	stroke(255);
	strokeWeight(5);
 point(this.x * Tile.Size - this.w / 2, this.y * Tile.Size - this.w / 2);
    point(this.x + this.w / 2, this.y - this.w / 2);
    point(this.x - this.w / 2, this.y + this.w / 2);
    point(this.x + this.w / 2, this.y + this.w / 2);
  }
  
}
