class AmbientSound{
	constructor(sound,tileX,tileY,radius){
		this.sound = Assets.get(sound);
		this.sound.setVolume(0);
		this.x = tileX;
		this.y = tileY;
		this.radius = radius;
	}
	
	display(){
		push();
		translate(this.x,this.y);
		noFill();
		strokeWeight(2);
		stroke(255);
		ellipse(this.x * Tile.Size,this.y * Tile.Size,this.radius * 2 * Tile.Size,this.radius * 2 * Tile.Size);
		noStroke();
		fill(255);
		textSize(12);
		text("Sound volume: " + this.sound.getVolume() ,this.x*Tile.Size - this.radius*Tile.Size,this.y*Tile.Size);
		
		pop();
	}
	
	followEntity(e){
		let dx = this.x - e.x;
		let dy = this.y - e.y;
		if(dx*dx + dy*dy <= this.radius*this.radius){
			let d = Math.sqrt(dx*dx + dy*dy);
			if(!this.sound.isPlaying()){
				this.sound.loop();
			}
			this.sound.setVolume(map(d,0,this.radius,1,0));
		}else{
			if(this.sound.isPlaying()){
				this.sound.setVolume(0);
			this.sound.stop();
			}
		}
	}
}