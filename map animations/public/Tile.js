class TileManager{
	
    static registerTileImage(id,img) {
        if (this.imagesTiles === undefined) {
            this.imagesTiles = {};
        }
		if(this.imagesTiles[id])
			return;
		this.imagesTiles[id] = img;
    }

    static getTileImage(id) {
        return this.imagesTiles[id];
    }

}

class Tile{
	
	static get Size(){
		return 32;
	}
	
	set id(val){
		this._id = val;
	}
	
	get id(){
		return this._id;
	}
	
	constructor(id,x,y){
		this.id = id;
		this.x = x;
		this.y = y;
	}
	
	render(){
		Map.drawTileImage(TileManager.getTileImage(this.id),this.x,this.y);
	}
}

class StaticTile extends Tile{
	constructor(id,x,y){
		super(id,x,y);
	}
}

class Entity{
	constructor(x,y,w,h){
		this.w = w;
		this.h = h;
		this.x = x;
		this.y = y;
	}
	update(dt){}
	render(){}
}



class TileAnimation{
	constructor(animationObject){
		this.currentFrameIndex = 0;
		this.currentTimer = 0;
		this.frames = animationObject;
		Map.addTileAnimation(this);
	}
	onNextFrame(){
		
	}
	nextFrame(){
		this.currentFrameIndex++;
		this.currentFrameIndex %= this.frames.length;
		this.currentTimer = 0;
		this.onNextFrame();
	}
	
	onNextFrame(){}
	
	update(dt){
		this.currentTimer += dt;
		if(this.currentTimer >= this.frames[this.currentFrameIndex].duration / 1000){
			this.nextFrame();
		}
	}
	
	getCurrentTileId(){
		return this.frames[this.currentFrameIndex].tileid;
	}
}

class AnimatedTile extends Tile{
	constructor(animationObject,x,y){
		super(null,x,y);
		this.animation = new TileAnimation(animationObject);
		this.animation.onNextFrame = ()=>{
		 Map.requestRender(this);	
		}	
	}
	get id(){
		return this.animation.getCurrentTileId();
	}
	set id(value){
		this._id = value;
	}

}